# Snooze Deployment Tool

Snooze is a deployment tool written in Java (although not pure Java) that can be used to deploy projects to multiple servers using SSH, subversion and git.

A common case for this is to deploy website code to multiple webservers.

*This project is a bit out of date, and I would now recommend using CloudPrirate or something along the lines of Capistrano.*

## Convensions

The assumption is made that a number of conventions are followed, which can be further extended with configuration on a per project basis.

This document describes the usage of release branches, this behaviour however can be disabled by setting the configuration option release-branches to false. By disabling release branches, tags are created directly from the main development branch.

### Authentication

Snooze makes the assumption that SSH keys are setup to allow password-less logins to the deployment servers and to subversion.

### Subversion

If the projects is using subversion, it should be maintained with the following layout.

| Description | Location  |
| - | - |
| Main development | /<repository>/<project>/trunk |
| Development branches | /<repository>/<project>/branches/<branch> |
| Release branches | /<repository>/<project>/branches/releases/<version> |
| Release tags | /<repository>/<project>/tags/<version> |

### Branch / Tag name format

Branches and tags should be maintained with the following format.

| Description | Format  |
| - | - |
| Development Branches | Any name not prefixed with "rc" |
| Release branches | rc1.0 |
| Release tags | 1.0 (or 1.0-p1 in the case of a patch release) |

### Deployment locations

Projects are deployed to one or more servers with the following mirrored layout. For a webserver the active directory (or subdirectory within) would be configured to be the document root.

The active directory provides a default layout, however this can be changed with configuration to provide one or more deployment locations per project.

When not configured to use symlinks, a switching directory is used to prepare the release.

| Description | Location  |
| - | - |
| Live directory | /<deployment root>/<project>/active |
| Switching directory | /<deployment root>/<project>/spare |

When configured to use symlinks, each release is exported as follows, and the live directory becomes a symlink.

| Description | Location  |
| - | - |
| Live directory | /<deployment root>/<project>/active |
| Development branches | /<deployment root>/<project>/mybranch/<version> |
| Release branches | /<deployment root>/<project>/branch/<version> |
| Release tags | /<deployment root>/<project>/tag/<version> |

### Deployment lifecycle

Deploying new features to production.

- A new release branch version number is created from the main development branch.
- The release branch is deployed to a testing environment.
- Any fixes or changes are made on the main development branch, merged into the release branch and then redeployed to the testing environment.
- A tag of the same version number is created from the release branch once that release branch has passed testing.
- The tag is deployed to the production environment.

Deploying bug fixes to production.

- Fixes are made on the main development branch.
- Those fixes (and only those fixes) are merged into the current release branch. The current release branch is the branch of which the currently deployed tag was derived from.
- The release branch is deployed to a testing environment.
- Any further fixes or changes are made on the main development branch, merged into the release branch and then redeployed to the testing environment.
- A new tag is created from the release branch once that release branch has passed testing. The version number remains the same, however "-pX" is appended, where X is an incremented integer.
- The tag is deployed to the production environment.

## Tasks

Snooze is comprised as a collection of tasks. The following core tasks are used to perform the above deployment lifecycle.

- Creation of a release branch from the development branch (trunk).
- Creation of a tag from a release branch.
- Updating of the switching directory to (or exporting) a specified tag.
- Deployment of the tag to the live directory, (the live and switching directories are swapped, or a symlink is updated depending on configuration).

The help task "./bin/release help" or "./bin/release help <task>" can be run to view further details.

## Configuration

Configuration is maintained within the file `./etc/conf.xml`. An example configuration is shown below.

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<release>

	<config>
		<!--
		The user to login with on the deployment servers,
		SSH keys should be set up to allow password-less logins.
		-->
		<remote-user>www</remote-user>

		<!--
		Use releases branches.
		If true, versioned release branches are created from trunk and tags
		from release branches. If false or omitted, tags are created from trunk.
		-->
		<release-branches>true</release-branches>

		<!--
		Use symlinks over active / spare directory switching.
		If true, the release is exported to the server, and a symlink is updated
		to point to the selected version.
		If false, a switching directory (named spare) is used to prepare the selected
		version and deployment is performed by swapping the selected deployment
		directory (defined in the layout, usually named active) with it.
		-->
		<symlinks>false</symlinks>

		<!--
		Location of where snooze may check out local copies of the project to perform
		branching and tagging operations. In the case of using git, this should never
		be a subdirectory of another git project, e.g. snooze itself.
		By default, the system temp directory is used.
		-->
		<local-repo>/var/snooze</local-repo>
	</config>

	<!-- A list of server names against IP addresses or hostnames -->
	<servers>
		<server>
			<name>web-1</name>
			<host>1.2.3.4</host>
		</server>
		<server>
			<name>web-2</name>
			<host>2.3.4.5</host>
		</server>
		<server>
			<name>test-web-1</name>
			<host>1.1.1.1</host>
		</server>
	</servers>

	<!--
	A logical grouping of servers,
	it is acceptable for a server to exist in multiple groups
	-->
	<clusters>
		<cluster name="prod" layout="prod">
			<server>web-1</server>
			<server>web-2</server>
		</cluster>
		<cluster name="stage" layout="stage">
			<server>test-web-1</server>
		</cluster>
	</clusters>

	<!-- A list of repositories -->
	<repos>
		<repo>
			<type>svn</type>
			<name>websites</name>
			<base>svn+ssh://localhost/svn</base>
			<main-branch>trunk</main-branch>
		</repo>
		<repo>
			<type>git</type>
			<name>snooze</name>
			<path>git@bitbucket.org:bedicus/snooze.git</path>
			<main-branch>master</main-branch>
		</repo>
	</repos>

	<!-- A list of deployment path names against base paths -->
	<deploy-paths>
		<deploy-path>
			<name>varwww</name>
			<base>/var/www</base>
		</deploy-path>
	</deploy-paths>

	<!--
	A list of possible directories that can be deployed to under each project
	deployment directory, this allows for multiple deployments per project.
	Each layout is applied to a cluster using the cluster layout attribute and
	if omitted, "active" is always used.
	-->
	<deploy-layouts>
		<layout name="prod">
			<dir>active</dir>
		</layout>
		<layout name="stage">
			<dir>slot1</dir>
			<dir>slot2</dir>
			<dir>slot3</dir>
		</layout>
	</deploy-layouts>

	<!--
	Project configuration, mapping to subversion repository,
	deployment directory and environments to deployment servers
	-->
	<projects>

		<project>
			<!--
			The svn path is svn+ssh://localhost/svn/my-project
			The deployment directory is /var/www/my-project
			Deploying to the stage environment deploys to servers: test-web-1
			Deploying to the prod environment deploys to servers: web-1, web-2
			-->
			<name>my-project</name>
			<repo>websites</repo>
			<deploy>varwww</deploy>
			<envs>
				<env name="stage">stage</env>
				<env name="prod">prod</env>
			</envs>
		</project>

		<project>
			<!--
			The svn path is svn+ssh://localhost/svn/foo/your-project
			The deployment directory is /var/www/foo/your-project
			Deploying to the foo-stage environment deploys to servers: test-web-1
			Deploying to the foo-prod environment deploys to servers: web-1, web-2
			-->
			<name>your-project</name>
			<repo>websites</repo>
			<deploy>varwww</deploy>
			<!-- repo-path is only available when the repo defines a base path -->
			<repo-path>/foo/your-project</repo-path>
			<deploy-path>/foo/your-project</deploy-path>

			<envs>
				<env name="foo-stage">stage</env>
				<env name="foo-prod">prod</env>
			</envs>

			<!--
			Only available with subversion repositories, a list of paths relative
			to the project in which svn:externals should be fixed to the current
			revision when creating a release branch.
			-->
			<externals>
				<external>plugins</external>
			</externals>

			<!--
			Commands to run pre and post switch,
			Run as part of the switch task, in the case where config symlinks
			is false. Either absolute, or relative to the switching directory.
			-->
			<pre-switch>bin/pre-switch</pre-switch>
			<post-switch>bin/post-switch</post-switch>

			<!--
			Commands to run post export,
			Run as part of the export task, in the case where config symlinks
			is true. Either absolute, or relative to the export directory.
			-->
			<post-export>bin/post-export</post-export>

			<!--
			Commands to run pre and post deploy,
			Either absolute, or relative to the live directory
			-->
			<pre-deploy>bin/pre-deploy</pre-deploy>
			<post-deploy>bin/post-deploy</post-deploy>

			<!--
			Commands to run pre and post switch,
			Run as part of the switch task, in the case where config symlinks
			is false. The commands are only run on the first defined server
			in the used cluster. Either absolute, or relative to the
			switching directory.
			-->
			<single-pre-switch>bin/single-pre-switch</single-pre-switch>
			<single-post-switch>bin/single-post-switch</single-post-switch>

			<!--
			Commands to run post export,
			Run as part of the export task, in the case where config symlinks
			is true. The commands are only run on the first defined server
			in the used cluster. Either absolute, or relative to the
			export directory.
			-->
			<single-post-export>bin/single-post-export</single-post-export>

			<!--
			Commands to run pre and post deploy,
			The commands are only run on the first defined server in the used cluster
			Either absolute, or relative to the live directory
			-->
			<single-pre-deploy>bin/single-pre-deploy</single-pre-deploy>
			<single-post-deploy>bin/single-post-deploy</single-post-deploy>
		</project>

	</projects>

	<!--
	Additional remote commands, each command has the following attributes,

	buffer [true|false] (default false)
		Whether to buffer the commands output before displaying it to the
		terminal or flush per line.
	parallel [true|false] (default true)
		Whether to run the commands in parallel or sequentially
		across machines.
	-->
	<remote-commands>
		<command buffer="true" parallel="true">
			<name>uptime</name>
			<exec>/usr/bin/uptime</exec>
		</command>
		<command buffer="true" parallel="true">
			<name>ps</name>
			<exec>/bin/ps aux</exec>
		</command>
	</remote-commands>

</release>
```

## Examples

Using the above configuration the following commands can be run to perform deployment tasks.

```bash
# Run Snooze interactively
./bin/release -i

# List projects snooze is aware of
> projects

# Create a release branch of the my-project project
# The version is auto incremented from the previous branch unless stated
> branch my-project

# Switch the switching directory for the my-project project on the testing
# environment to prepare for deployment of branch 1.0
> switch my-project stage branch 1.0

# Deploy the switching directory to the live directory on the testing environment
> deploy my-project stage

# Create a tag from the release branch
# The tag version is taken from the latest release branch version unless stated
> tag my-project

# Switch the switching directory for the my-project project on the production
# environment to prepare for deployment of tag 1.0
> switch my-project prod tag 1.0

# Deploy the switching directory to the live directory on the production environment
> deploy my-project prod

# Check uptime for all the servers for the my-project project
# on the production environment
> uptime my-project prod

# List projects snooze is aware of, running non-interactively
./bin/release projects
```

## Open Source

Snooze makes use of the following open source libraries, any changes made will be listed here for download.

- [JSch](http://www.jcraft.com/jsch/)
