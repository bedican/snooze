/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze;

import java.io.*;
import java.util.*;
import java.text.*;

import com.jcraft.jsch.*;

public class DistributedShellExec
{
	private String user = null;
	private String password = null;
	private boolean parallelExec = true;
	private Map<String, String> hosts = new HashMap<String, String>();
	private List<ShellExecCommand> commands = new Vector<ShellExecCommand>();

	public DistributedShellExec()
	{
	}
	public DistributedShellExec(ShellExecCommand command)
	{
		this.addCommand(command);
	}

	public void addCommand(ShellExecCommand command)
	{
		if(command == null) {
			throw new NullPointerException();
		}

		this.commands.add(command);
	}

	public void setUser(String user)
	{
		if(user == null) {
			throw new NullPointerException();
		}
		if(user.isEmpty()) {
			throw new IllegalArgumentException("Empty user");
		}

		this.user = user;
	}
	public void setPassword(String password)
	{
		// Allow null
		this.password = password;
	}

	public void setParallelExec(boolean parallelExec)
	{
		this.parallelExec = parallelExec;
	}

	public void addHost(String name, String host)
	{
		if((name == null) || (host == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}
		if(host.isEmpty()) {
			throw new IllegalArgumentException("Missing host");
		}

		this.hosts.put(name.toLowerCase(), host);
	}

	public void execute(PrintStream out) throws Exception
	{
		if(this.user == null) {
			throw new IllegalStateException("No user has been set");
		}

		int maxLength = 0;
		for(String server:this.hosts.keySet()) {
			if(server.length() > maxLength) {
				maxLength = server.length();
			}
		}

		ShellExecFormatter formatter = new ShellExecFormatter(out, maxLength);
		List<ShellExecThread> threads = new Vector<ShellExecThread>();
		
		for(String server:this.hosts.keySet()) {
			threads.add(new ShellExecThread(server, this.hosts.get(server), threads.isEmpty(), this.user, this.password, this.commands, formatter));
		}

		if(this.parallelExec) {
			this.executeThreadsParallel(threads);
		} else {
			this.executeThreadsSerial(threads);
		}

		boolean failed = false;
		for(ShellExecThread thread:threads) {
			failed = failed || thread.failed();
		}

		if(failed) // Rollback all threads that did not fail
		{
			List<ShellExecThread> rollbackThreads = new Vector<ShellExecThread>();

			for(ShellExecThread thread:threads) {
				if(!thread.failed()) {
					rollbackThreads.add(new ShellExecThread(thread.getServer(), this.hosts.get(thread.getServer()), thread.isFirstServer(), this.user, this.password, this.commands, formatter, true));
				}				
			}

			if(this.parallelExec) {
				this.executeThreadsParallel(rollbackThreads);
			} else {
				this.executeThreadsSerial(rollbackThreads);
			}
		}
	}

	private void executeThreadsParallel(List<ShellExecThread> threads) throws Exception
	{
		for(ShellExecThread thread:threads) {
			thread.start();
		}
		for(ShellExecThread thread:threads) {
			thread.join();
		}
	}

	private void executeThreadsSerial(List<ShellExecThread> threads) throws Exception
	{
		for(ShellExecThread thread:threads) {
			thread.start();
			thread.join();
		}
	}

	private static class ShellExecFormatter
	{
		private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

		private Formatter formatter;
		private String format;

		public ShellExecFormatter(PrintStream out, int labelLength)
		{
			this.formatter = new Formatter(out);

			StringBuffer buffer = new StringBuffer();
			buffer.append(" [ %-").append(labelLength).append("s ] (%s) %s\n");
			this.format = buffer.toString();
		}

		public void format(String label, String message)
		{
			synchronized(this.formatter)
			{
				for(String line:message.trim().split("\n")) {
					if(!line.trim().isEmpty()) {
						this.formatter.format(this.format, new Object[] { label, this.sdf.format(new Date()), line.trim() });
					}
				}
			}
		}
	}

	private static class ShellExecOutputStream extends ByteArrayOutputStream
	{
		private ShellExecFormatter formatter;
		private String label;
		private boolean bufferOutput;
		private PrintStream printStream;

		public ShellExecOutputStream(ShellExecFormatter formatter, String label, boolean bufferOutput)
		{
			this.formatter = formatter;
			this.label = label;
			this.bufferOutput = bufferOutput;

			this.printStream = new PrintStream(this, true);
		}

		public void println(String line)
		{
			this.printStream.println(line);
		}

		private void flushToFormatter()
		{
			this.formatter.format(this.label, this.toString());
			this.reset();
		}

		public void flush() throws IOException
		{
			super.flush();

			if(!this.bufferOutput) {
				this.flushToFormatter();
			}
		}

		public void close() throws IOException
		{
			this.printStream.close();
			this.flushToFormatter();
			super.close();
		}
	}

	private static class ShellExecThread extends Thread
	{
		private ShellExecFormatter formatter;
		private List<ShellExecCommand> commands;

		private String server;
		private String host;
		private String user;
		private String password;

		private Session session = null;
		private boolean rollback = false;
		private boolean firstServer = false;

		private int index = 0;
		private boolean failed = false;

		public ShellExecThread(String server, String host, boolean firstServer, String user, String password, List<ShellExecCommand> commands, ShellExecFormatter formatter)
		{
			this(server, host, firstServer, user, password, commands, formatter, false);
		}

		public ShellExecThread(String server, String host, boolean firstServer, String user, String password, List<ShellExecCommand> commands, ShellExecFormatter formatter, boolean rollback)
		{
			this.server = server;
			this.host = host;
			this.user = user;
			this.password = password;
			this.formatter = formatter;
			this.rollback = rollback;
			this.firstServer = firstServer;
			this.commands = new Vector<ShellExecCommand>();
			
			// Not all commands should be run on all servers.
			for(ShellExecCommand command:commands) {
				if((this.firstServer) || (!command.getSingleServerMode())) {
					this.commands.add(command);
				}
			}
		}

		public String getServer()
		{
			return this.server;
		}

		public boolean failed()
		{
			return this.failed;
		}
		
		public boolean isFirstServer()
		{
			return this.firstServer;
		}

		private void initSession() throws JSchException, FileNotFoundException
		{
			JSch jsch = new JSch();

			File sshDir = new File(System.getProperty("user.home"), ".ssh");

			File knownHosts = new File(sshDir, "known_hosts");
			File privateKey = new File(sshDir, "id_rsa");

			if(!knownHosts.isFile()) {
				throw new FileNotFoundException("Missing known hosts: " + knownHosts.getAbsolutePath());
			}
			if(!privateKey.isFile()) {
				throw new FileNotFoundException("Missing private key: " + privateKey.getAbsolutePath());
			}

			jsch.setKnownHosts(knownHosts.getAbsolutePath());
			jsch.addIdentity(privateKey.getAbsolutePath());

			this.session = jsch.getSession(this.user, this.host, 22);

			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			this.session.setConfig(config);
			
			if(this.password != null) { // Password is optional, it is preferable to use ssh keys.
				session.setPassword(this.password);
			}

			this.session.connect();
		}

		private void closeSession()
		{
			if(this.session != null) {
				this.session.disconnect();
			}
		}

		public void run()
		{
			try
			{
				this.initSession();

				if(this.rollback)
				{
					this.doRollback();
					return;
				}

				try
				{
					this.doRun();
				}
				catch(Exception e)
				{
					this.failed = true;
					this.formatter.format(this.server, e.getMessage());
					this.doRollback();
				}
			}
			catch(Exception e)
			{
				this.formatter.format(this.server, e.getMessage());
			}
			finally
			{
				this.closeSession();
			}
		}

		private void doRun() throws Exception
		{
			// All exceptions thrown here are due to the command not being run successfully.
			// This is an important disctinction when concerning rolling back due to exceptions.

			ShellExecOutputStream out;

			ShellExecCommand command;
			ChannelExec channel;
			int exitStatus;

			this.index = 0;

			while(this.index < this.commands.size())
			{
				command = this.commands.get(this.index);

				out = new ShellExecOutputStream(this.formatter, this.server, command.getOutputBuffering());

				out.println("$ " + command.getCommand());

				channel = (ChannelExec)session.openChannel("exec");
				channel.setCommand(command.getCommand());

				channel.setOutputStream(out);
				channel.setExtOutputStream(out);
				channel.setErrStream(out);

				channel.connect();

				while(!channel.isClosed()) {
					try { Thread.sleep(500); } catch(InterruptedException ie) {}
				}

				exitStatus = channel.getExitStatus();
				channel.disconnect();

				if((exitStatus != 0) && (command.getStopOnError())) {
					throw new Exception("Failed to run " + command.getCommand());
				}

				this.index++;
			}
		}

		private void doRollback() throws Exception
		{
			if(this.rollback) {
				this.index = this.commands.size();
			}

			ShellExecOutputStream out;

			ShellExecCommand command;
			ChannelExec channel;
			int exitStatus;

			while(this.index > 0)
			{
				this.index--;

				command = this.commands.get(this.index);

				if(!command.canRollback()) {
					continue;
				}

				out = new ShellExecOutputStream(this.formatter, this.server, command.getOutputBuffering());

				out.println("$ " + command.getRollbackCommand());

				channel = (ChannelExec)session.openChannel("exec");
				channel.setCommand(command.getRollbackCommand());

				channel.setOutputStream(out);
				channel.setExtOutputStream(out);
				channel.setErrStream(out);

				channel.connect();

				while(!channel.isClosed()) {
					try { Thread.sleep(500); } catch(InterruptedException ie) {}
				}

				exitStatus = channel.getExitStatus();
				channel.disconnect();

				if(exitStatus != 0) {
					throw new Exception("Failed to rollback on command " + command.getRollbackCommand());
				}
			}
		}
	}
}