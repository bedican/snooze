/**
 * (c) Copyright 2012 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.config;

import uk.co.bedican.snooze.*;

public class RepositoryConfig
{
	private String name;
	private RepositoryType type;
	private String base;
	private String path;
	private String branch;
	
	public RepositoryConfig(String repositoryName)
	{
		if(repositoryName == null) {
			throw new NullPointerException();
		}
		if(repositoryName.isEmpty()) {
			throw new IllegalArgumentException("Missing repository name");
		}
		
		this.name = repositoryName;
		
		this.type = RepositoryType.SVN;
		this.branch = "trunk";

		this.path = "";
		this.base = "";
	}

	public boolean hasBasePath()
	{
		return (!this.base.isEmpty());
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getBase()
	{
		return this.base;
	}

	public String getPath()
	{
		return this.path;
	}
	
	public RepositoryType getType()
	{
		return this.type;
	}
	
	public String getBranch()
	{
		return this.branch;
	}	
	
	public void setType(RepositoryType type)
	{
		if(type == null) {
			throw new NullPointerException();
		}
		
		this.type = type;
	}
	
	public void setBranch(String branch)
	{
		if(branch == null) {
			throw new NullPointerException();
		}
		if(branch.isEmpty()) {
			throw new IllegalArgumentException("Missing branch");
		}
		
		this.branch = branch;
	}

	public void setBase(String base)
	{
		if(base == null) {
			throw new NullPointerException();
		}
		if(base.isEmpty()) {
			throw new IllegalArgumentException("Missing base");
		}
		
		this.base = base;
	}

	public void setPath(String path)
	{
		if(path == null) {
			throw new NullPointerException();
		}
		if(path.isEmpty()) {
			throw new IllegalArgumentException("Missing path");
		}
		
		this.path = path;
	}
}