/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.config;

import uk.co.bedican.snooze.*;

public class ConfigException extends Exception
{
	public ConfigException(String message) { super(message); }
	public ConfigException(String message, Throwable t) { super(message, t); }
	public ConfigException(Throwable t) { this(t.getMessage(), t); }
}