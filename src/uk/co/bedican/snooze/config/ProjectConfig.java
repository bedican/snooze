/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.config;

import uk.co.bedican.snooze.*;
import java.util.*;

public class ProjectConfig
{
	private String name;
	private String repositoryName;
	private String deploymentName;

	private String repositoryPath = "";
	private String deploymentPath = "";

	private String preDeployExec = "";
	private String postDeployExec = "";
	private String preSwitchExec = "";
	private String postSwitchExec = "";
	
	private String singlePreDeployExec = "";
	private String singlePostDeployExec = "";
	private String singlePreSwitchExec = "";
	private String singlePostSwitchExec = "";
	
	private String postExportExec = "";
	private String singlePostExportExec = "";

	private Map<String, String> environments = new HashMap<String, String>();
	private List<String> externals = new Vector<String>();

	public ProjectConfig(String name, String repositoryName, String deploymentName)
	{
		if((name == null) || (repositoryName == null) || (deploymentName == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Empty name");
		}
		if(repositoryName.isEmpty()) {
			throw new IllegalArgumentException("Empty repository name");
		}
		if(deploymentName.isEmpty()) {
			throw new IllegalArgumentException("Empty deployment name");
		}

		this.name = name;
		this.repositoryName = repositoryName;
		this.deploymentName = deploymentName;
	}

	public String getName()
	{
		return this.name;
	}

	public String getDeploymentName()
	{
		return this.deploymentName;
	}

	public String getDeploymentPath()
	{
		return this.deploymentPath;
	}

	public String getRepositoryName()
	{
		return this.repositoryName;
	}

	public String getRepositoryPath()
	{
		return this.repositoryPath;
	}

	public String getPreDeployExec()
	{
		return this.preDeployExec;
	}

	public String getPostDeployExec()
	{
		return this.postDeployExec;
	}

	public String getPreSwitchExec()
	{
		return this.preSwitchExec;
	}

	public String getPostSwitchExec()
	{
		return this.postSwitchExec;
	}

	public String getSinglePreDeployExec()
	{
		return this.singlePreDeployExec;
	}
	
	public String getSinglePostDeployExec()
	{
		return this.singlePostDeployExec;
	}
	
	public String getSinglePreSwitchExec()
	{
		return this.singlePreSwitchExec;
	}
	
	public String getSinglePostSwitchExec()
	{
		return this.singlePostSwitchExec;
	}
	
	public String getPostExportExec()
	{
		return this.postExportExec;
	}
	
	public String getSinglePostExportExec()
	{
		return this.singlePostExportExec;
	}

	public Map<String, String> getEnvironments()
	{
		return Collections.unmodifiableMap(this.environments);
	}

	public String getEnvironment(String environment)
	{
		if(environment == null) {
			throw new NullPointerException();
		}
		if(environment.isEmpty()) {
			throw new IllegalArgumentException("Empty environment");
		}

		return this.environments.get(environment);
	}

	public List<String> getExternals()
	{
		return Collections.unmodifiableList(this.externals);
	}

	public void setRepositoryPath(String path)
	{
		if(path == null) {
			throw new NullPointerException();
		}
		if(path.isEmpty()) {
			throw new IllegalArgumentException("Empty path");
		}

		this.repositoryPath = path;
	}

	public void setDeploymentPath(String path)
	{
		if(path == null) {
			throw new NullPointerException();
		}
		if(path.isEmpty()) {
			throw new IllegalArgumentException("Empty path");
		}

		this.deploymentPath = path;
	}

	public void setPreDeployExec(String exec)
	{
		if(exec == null) {
			throw new NullPointerException();
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Empty exec");
		}

		this.preDeployExec = exec;
	}

	public void setPostDeployExec(String exec)
	{
		if(exec == null) {
			throw new NullPointerException();
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Empty exec");
		}

		this.postDeployExec = exec;
	}

	public void setPreSwitchExec(String exec)
	{
		if(exec == null) {
			throw new NullPointerException();
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Empty exec");
		}

		this.preSwitchExec = exec;
	}

	public void setPostSwitchExec(String exec)
	{
		if(exec == null) {
			throw new NullPointerException();
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Empty exec");
		}

		this.postSwitchExec = exec;
	}	
	
	public void setSinglePreDeployExec(String exec)
	{
		if(exec == null) {
			throw new NullPointerException();
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Empty exec");
		}

		this.singlePreDeployExec = exec;
	}

	public void setSinglePostDeployExec(String exec)
	{
		if(exec == null) {
			throw new NullPointerException();
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Empty exec");
		}

		this.singlePostDeployExec = exec;
	}

	public void setSinglePreSwitchExec(String exec)
	{
		if(exec == null) {
			throw new NullPointerException();
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Empty exec");
		}

		this.singlePreSwitchExec = exec;
	}

	public void setSinglePostSwitchExec(String exec)
	{
		if(exec == null) {
			throw new NullPointerException();
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Empty exec");
		}

		this.singlePostSwitchExec = exec;
	}
	
	public void setPostExportExec(String exec)
	{
		if(exec == null) {
			throw new NullPointerException();
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Empty exec");
		}

		this.postExportExec = exec;
	}
	
	public void setSinglePostExportExec(String exec)
	{
		if(exec == null) {
			throw new NullPointerException();
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Empty exec");
		}

		this.singlePostExportExec = exec;
	}

	public void addEnvironment(String environment, String cluster)
	{
		if((environment == null) || (cluster == null)) {
			throw new NullPointerException();
		}
		if(environment.isEmpty()) {
			throw new IllegalArgumentException("Empty environment");
		}
		if(cluster.isEmpty()) {
			throw new IllegalArgumentException("Empty cluster");
		}

		this.environments.put(environment, cluster);
	}

	public void addExternal(String external)
	{
		if(external == null) {
			throw new NullPointerException();
		}
		if(external.isEmpty()) {
			throw new IllegalArgumentException("Empty external");
		}
		if(this.externals.contains(external)) {
			return;
		}

		this.externals.add(external);
	}
}