/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.config;

import uk.co.bedican.snooze.*;
import java.net.*;
import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.w3c.dom.*;

public class ConfigBuilder
{
	private XPath xp;
	private DocumentBuilder documentBuilder;

	public ConfigBuilder() throws ConfigException
	{
		try
		{
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilderFactory.setNamespaceAware(true);
			this.documentBuilder = documentBuilderFactory.newDocumentBuilder();

			this.xp = XPathFactory.newInstance().newXPath();
		}
		catch(Exception e)
		{
			throw new ConfigException("Failed to create document builder.", e);
		}
	}

	public Config build(String filename) throws ConfigException
	{
		if(filename == null) {
			throw new NullPointerException();
		}
		if(filename.isEmpty()) {
			throw new IllegalArgumentException("Missing filename");
		}

		File file = new File(filename);

		if(!file.isFile()) {
			throw new ConfigException(new FileNotFoundException("File " + filename + " does not exist"));
		}

		Config config = new Config();

		Document xmldoc;

		try
		{
			xmldoc = this.documentBuilder.parse(file);

			this.addConfiguration(xmldoc, config);
			this.addDeploymentLayouts(xmldoc, config);
			this.addServers(xmldoc, config);
			this.addClusters(xmldoc, config);
			this.addRepositories(xmldoc, config);
			this.addDeploymentPaths(xmldoc, config);
			this.addProjects(xmldoc, config);
			this.addRemoteCommands(xmldoc, config);
		}
		catch(Exception e)
		{
			throw new ConfigException("Failed to parse configuration file: " + e.getMessage(), e);
		}

		return config;
	}

	private void addConfiguration(Document xmldoc, Config config) throws Exception
	{
		String remoteUser = (String)this.xp.compile("/release/config/remote-user/text()").evaluate(xmldoc, XPathConstants.STRING);
		if(!remoteUser.isEmpty()) {
			config.setRemoteUser(remoteUser);
		}

		String remotePassword = (String)this.xp.compile("/release/config/remote-password/text()").evaluate(xmldoc, XPathConstants.STRING);
		if(!remotePassword.isEmpty()) {
			config.setRemotePassword(remotePassword);
		}

		String idleTime = (String)this.xp.compile("/release/config/idle-time/text()").evaluate(xmldoc, XPathConstants.STRING);
		if(!idleTime.isEmpty()) {
			config.setIdleTime(Integer.parseInt(idleTime));
		}

		String localRepoDir = (String)this.xp.compile("/release/config/local-repo/text()").evaluate(xmldoc, XPathConstants.STRING);
		if(!localRepoDir.isEmpty()) {
			File localRepo = new File(localRepoDir);
			if(!localRepo.isAbsolute()) {
				throw new ConfigException("config local-repo must be absolute");
			}

			config.setLocalRepositoriesDirectory(localRepo);
		}
		
		String useReleaseBranches = (String)this.xp.compile("/release/config/release-branches/text()").evaluate(xmldoc, XPathConstants.STRING);
		config.setUseReleaseBranches(useReleaseBranches.equals("true"));
		
		String useSymlinks = (String)this.xp.compile("/release/config/symlinks/text()").evaluate(xmldoc, XPathConstants.STRING);
		config.setUseSymlinks(useSymlinks.equals("true"));
	}

	private void addDeploymentLayouts(Document xmldoc, Config config) throws Exception
	{
		NodeList layouts = (NodeList)this.xp.compile("/release/deploy-layouts/layout").evaluate(xmldoc,  XPathConstants.NODESET);

		XPathExpression xpLayoutName = this.xp.compile("@name");
		XPathExpression xpLayoutDirectories = this.xp.compile("dir/text()");

		String layoutName, directory;
		NodeList layoutDirectories;
		List<String> directories;

		for(int x = 0; x < layouts.getLength(); x++)
		{
			layoutName = (String)xpLayoutName.evaluate(layouts.item(x), XPathConstants.STRING);

			if(layoutName.isEmpty()) {
				continue;
			}

			layoutDirectories = (NodeList)xpLayoutDirectories.evaluate(layouts.item(x), XPathConstants.NODESET);

			directories = new Vector<String>();

			for(int y = 0; y < layoutDirectories.getLength(); y++)
			{
				directory = layoutDirectories.item(y).getNodeValue();

				if(directory.isEmpty()) {
					continue;
				}

				directories.add(directory);
			}

			if(directories.isEmpty()) {
				continue;
			}

			config.addDeploymentLayout(layoutName, directories);
		}
	}

	private void addServers(Document xmldoc, Config config) throws Exception
	{
		NodeList servers = (NodeList)this.xp.compile("/release/servers/server").evaluate(xmldoc,  XPathConstants.NODESET);

		XPathExpression xpServerName = this.xp.compile("name/text()");
		XPathExpression xpServerHost = this.xp.compile("host/text()");

		String serverName, serverHost;

		for(int x = 0; x < servers.getLength(); x++)
		{
			serverName = (String)xpServerName.evaluate(servers.item(x), XPathConstants.STRING);
			serverHost = (String)xpServerHost.evaluate(servers.item(x), XPathConstants.STRING);

			if((serverName.isEmpty()) || (serverHost.isEmpty())) {
				continue;
			}

			config.addServer(serverName, serverHost);
		}
	}

	private void addClusters(Document xmldoc, Config config) throws Exception
	{
		NodeList clusters = (NodeList)this.xp.compile("/release/clusters/cluster").evaluate(xmldoc,  XPathConstants.NODESET);

		XPathExpression xpClusterName = this.xp.compile("@name");
		XPathExpression xpClusterLayout = this.xp.compile("@layout");
		XPathExpression xpClusterServers = this.xp.compile("server/text()");

		String clusterName, clusterServer, clusterLayout;
		NodeList clusterServers;

		for(int x = 0; x < clusters.getLength(); x++)
		{
			clusterName = (String)xpClusterName.evaluate(clusters.item(x), XPathConstants.STRING);

			if(clusterName.isEmpty()) {
				continue;
			}

			clusterServers = (NodeList)xpClusterServers.evaluate(clusters.item(x), XPathConstants.NODESET);

			for(int y = 0; y < clusterServers.getLength(); y++)
			{
				clusterServer = clusterServers.item(y).getNodeValue();

				if(clusterServer.isEmpty()) {
					continue;
				}

				config.addServerToCluster(clusterName, clusterServer);
			}

			clusterLayout = (String)xpClusterLayout.evaluate(clusters.item(x), XPathConstants.STRING);

			if(clusterLayout.isEmpty()) {
				continue;
			}

			config.setClusterLayout(clusterName, clusterLayout);
		}
	}

	private void addRepositories(Document xmldoc, Config config) throws Exception
	{
		NodeList repos = (NodeList)this.xp.compile("/release/repos/repo").evaluate(xmldoc,  XPathConstants.NODESET);

		XPathExpression xpRepoName = this.xp.compile("name/text()");
		XPathExpression xpRepoType = this.xp.compile("type/text()");
		XPathExpression xpRepoBase = this.xp.compile("base/text()");
		XPathExpression xpRepoPath = this.xp.compile("path/text()");
		XPathExpression xpRepoBranch = this.xp.compile("main-branch/text()");

		String repoName, repoType, repoBase, repoPath, repoBranch;

		for(int x = 0; x < repos.getLength(); x++)
		{
			repoName = (String)xpRepoName.evaluate(repos.item(x), XPathConstants.STRING);
			repoType = (String)xpRepoType.evaluate(repos.item(x), XPathConstants.STRING);
			repoBase = (String)xpRepoBase.evaluate(repos.item(x), XPathConstants.STRING);
			repoPath = (String)xpRepoPath.evaluate(repos.item(x), XPathConstants.STRING);
			repoBranch = (String)xpRepoBranch.evaluate(repos.item(x), XPathConstants.STRING);

			if(repoName.isEmpty()) {
				continue;
			}
			if((repoBase.isEmpty()) && (repoPath.isEmpty())) {
				continue;
			}
			
			RepositoryConfig repoConfig = new RepositoryConfig(repoName);

			// Try to infer type
			if((repoType.isEmpty()) && (repoPath.endsWith(".git"))) {
				repoType = "git";
			}

			// Try to infer branch
			if((repoType.equals("git")) && (repoBranch.isEmpty())) {
				repoBranch = "master";
			}

			if(!repoType.isEmpty()) {
				RepositoryType type = RepositoryType.getType(repoType);
				if(type != null) {
					repoConfig.setType(type);
				}
			}
			
			if(!repoBranch.isEmpty()) {
				repoConfig.setBranch(repoBranch);
			}

			if(!repoPath.isEmpty()) {
				repoConfig.setPath(repoPath);
			} else if(!repoBase.isEmpty()) {
				repoConfig.setBase(repoBase);
			}
			
			config.addRepositoryConfig(repoConfig);
		}
	}

	private void addDeploymentPaths(Document xmldoc, Config config) throws Exception
	{
		NodeList paths = (NodeList)this.xp.compile("/release/deploy-paths/deploy-path").evaluate(xmldoc,  XPathConstants.NODESET);

		XPathExpression xpPathName = this.xp.compile("name/text()");
		XPathExpression xpPathBase = this.xp.compile("base/text()");

		String pathName, pathBase;

		for(int x = 0; x < paths.getLength(); x++)
		{
			pathName = (String)xpPathName.evaluate(paths.item(x), XPathConstants.STRING);
			pathBase = (String)xpPathBase.evaluate(paths.item(x), XPathConstants.STRING);

			if((pathName.isEmpty()) || (pathBase.isEmpty())) {
				continue;
			}

			config.addDeploymentPath(pathName, pathBase);
		}
	}

	private void addProjects(Document xmldoc, Config config) throws Exception
	{
		NodeList projects = (NodeList)this.xp.compile("/release/projects/project").evaluate(xmldoc,  XPathConstants.NODESET);

		XPathExpression xpProjectName = this.xp.compile("name/text()");
		XPathExpression xpProjectRepo = this.xp.compile("repo/text()");
		XPathExpression xpProjectDeploy = this.xp.compile("deploy/text()");
		XPathExpression xpProjectRepoPath = this.xp.compile("repo-path/text()");
		XPathExpression xpProjectDeployPath = this.xp.compile("deploy-path/text()");
		XPathExpression xpProjectPreDeploy = this.xp.compile("pre-deploy/text()");
		XPathExpression xpProjectPostDeploy = this.xp.compile("post-deploy/text()");
		XPathExpression xpProjectPreSwitch = this.xp.compile("pre-switch/text()");
		XPathExpression xpProjectPostSwitch = this.xp.compile("post-switch/text()");
		XPathExpression xpProjectPostExport = this.xp.compile("post-export/text()");
		XPathExpression xpProjectSinglePreDeploy = this.xp.compile("single-pre-deploy/text()");
		XPathExpression xpProjectSinglePostDeploy = this.xp.compile("single-post-deploy/text()");
		XPathExpression xpProjectSinglePreSwitch = this.xp.compile("single-pre-switch/text()");
		XPathExpression xpProjectSinglePostSwitch = this.xp.compile("single-post-switch/text()");
		XPathExpression xpProjectSinglePostExport = this.xp.compile("single-post-export/text()");
		XPathExpression xpProjectEnvs = this.xp.compile("envs/env");
		XPathExpression xpProjectEnvName = this.xp.compile("@name");
		XPathExpression xpProjectEnvCluster = this.xp.compile("text()");
		XPathExpression xpProjectExternals = this.xp.compile("externals/external");
		XPathExpression xpProjectExternal = this.xp.compile("text()");

		String projectName, projectRepo, projectDeploy, projectRepoPath, projectDeployPath;
		String projectPreDeploy, projectPostDeploy, projectPreSwitch, projectPostSwitch;
		String singleProjectPreDeploy, singleProjectPostDeploy, singleProjectPreSwitch, singleProjectPostSwitch;
		String projectPostExport, singleProjectPostExport;
		String projectEnvName, projectEnvCluster, projectExternal;
		NodeList projectEnvs, projectExternals;

		for(int x = 0; x < projects.getLength(); x++)
		{
			projectName = (String)xpProjectName.evaluate(projects.item(x), XPathConstants.STRING);
			projectRepo = (String)xpProjectRepo.evaluate(projects.item(x), XPathConstants.STRING);
			projectDeploy = (String)xpProjectDeploy.evaluate(projects.item(x), XPathConstants.STRING);

			if((projectName.isEmpty()) || (projectRepo.isEmpty()) || (projectDeploy.isEmpty())) {
				continue;
			}

			projectEnvs = (NodeList)xpProjectEnvs.evaluate(projects.item(x), XPathConstants.NODESET);

			if(projectEnvs.getLength() == 0) {
				continue;
			}

			ProjectConfig projectConfig = new ProjectConfig(projectName, projectRepo, projectDeploy);

			projectRepoPath = (String)xpProjectRepoPath.evaluate(projects.item(x), XPathConstants.STRING);
			projectDeployPath = (String)xpProjectDeployPath.evaluate(projects.item(x), XPathConstants.STRING);

			projectPreDeploy = (String)xpProjectPreDeploy.evaluate(projects.item(x), XPathConstants.STRING);
			projectPostDeploy = (String)xpProjectPostDeploy.evaluate(projects.item(x), XPathConstants.STRING);

			projectPreSwitch = (String)xpProjectPreSwitch.evaluate(projects.item(x), XPathConstants.STRING);
			projectPostSwitch = (String)xpProjectPostSwitch.evaluate(projects.item(x), XPathConstants.STRING);
			
			singleProjectPreDeploy = (String)xpProjectSinglePreDeploy.evaluate(projects.item(x), XPathConstants.STRING);
			singleProjectPostDeploy = (String)xpProjectSinglePostDeploy.evaluate(projects.item(x), XPathConstants.STRING);

			singleProjectPreSwitch = (String)xpProjectSinglePreSwitch.evaluate(projects.item(x), XPathConstants.STRING);
			singleProjectPostSwitch = (String)xpProjectSinglePostSwitch.evaluate(projects.item(x), XPathConstants.STRING);
			
			projectPostExport = (String)xpProjectPostExport.evaluate(projects.item(x), XPathConstants.STRING);
			singleProjectPostExport = (String)xpProjectSinglePostExport.evaluate(projects.item(x), XPathConstants.STRING);

			projectExternals = (NodeList)xpProjectExternals.evaluate(projects.item(x), XPathConstants.NODESET);

			if(!projectRepoPath.isEmpty()) {
				projectConfig.setRepositoryPath(projectRepoPath);
			}
			if(!projectDeployPath.isEmpty()) {
				projectConfig.setDeploymentPath(projectDeployPath);
			}
			if(!projectPreDeploy.isEmpty()) {
				projectConfig.setPreDeployExec(projectPreDeploy);
			}
			if(!projectPostDeploy.isEmpty()) {
				projectConfig.setPostDeployExec(projectPostDeploy);
			}
			if(!projectPreSwitch.isEmpty()) {
				projectConfig.setPreSwitchExec(projectPreSwitch);
			}
			if(!projectPostSwitch.isEmpty()) {
				projectConfig.setPostSwitchExec(projectPostSwitch);
			}
			if(!singleProjectPreDeploy.isEmpty()) {
				projectConfig.setSinglePreDeployExec(singleProjectPreDeploy);
			}
			if(!singleProjectPostDeploy.isEmpty()) {
				projectConfig.setSinglePostDeployExec(singleProjectPostDeploy);
			}
			if(!singleProjectPreSwitch.isEmpty()) {
				projectConfig.setSinglePreSwitchExec(singleProjectPreSwitch);
			}
			if(!singleProjectPostSwitch.isEmpty()) {
				projectConfig.setSinglePostSwitchExec(singleProjectPostSwitch);
			}
			if(!projectPostExport.isEmpty()) {
				projectConfig.setPostExportExec(projectPostExport);
			}
			if(!singleProjectPostExport.isEmpty()) {
				projectConfig.setSinglePostExportExec(singleProjectPostExport);
			}

			for(int y = 0; y < projectEnvs.getLength(); y++)
			{
				projectEnvName = (String)xpProjectEnvName.evaluate(projectEnvs.item(y), XPathConstants.STRING);
				projectEnvCluster = (String)xpProjectEnvCluster.evaluate(projectEnvs.item(y), XPathConstants.STRING);

				if((projectEnvName.isEmpty()) || (projectEnvCluster.isEmpty())) {
					continue;
				}

				projectConfig.addEnvironment(projectEnvName, projectEnvCluster);
			}

			for(int z = 0; z < projectExternals.getLength(); z++)
			{
				projectExternal = (String)xpProjectExternal.evaluate(projectExternals.item(z), XPathConstants.STRING);

				if(!projectExternal.isEmpty()) {
					projectConfig.addExternal(projectExternal);
				}
			}

			config.addProjectConfig(projectConfig);
		}
	}

	private void addRemoteCommands(Document xmldoc, Config config) throws Exception
	{
		NodeList commands = (NodeList)this.xp.compile("/release/remote-commands/command").evaluate(xmldoc,  XPathConstants.NODESET);

		XPathExpression xpCommandBuffer = this.xp.compile("@buffer");
		XPathExpression xpCommandParallel = this.xp.compile("@parallel");
		XPathExpression xpCommandName = this.xp.compile("name/text()");
		XPathExpression xpCommandExec = this.xp.compile("exec/text()");

		String commandBuffer, commandParallel, commandName, commandExec;
		RemoteCommandConfig commandConfig;

		for(int x = 0; x < commands.getLength(); x++)
		{
			commandBuffer = (String)xpCommandBuffer.evaluate(commands.item(x), XPathConstants.STRING);
			commandParallel = (String)xpCommandParallel.evaluate(commands.item(x), XPathConstants.STRING);
			commandName = (String)xpCommandName.evaluate(commands.item(x), XPathConstants.STRING);
			commandExec = (String)xpCommandExec.evaluate(commands.item(x), XPathConstants.STRING);

			if((commandName.isEmpty()) || (commandExec.isEmpty())) {
				continue;
			}

			commandConfig = new RemoteCommandConfig(commandName, commandExec);

			if(!commandBuffer.isEmpty()) {
				if(commandBuffer.equals("true")) {
					commandConfig.setOutputBuffering(true);
				} else if(commandBuffer.equals("false")) {
					commandConfig.setOutputBuffering(false);
				}
			}
			if(!commandParallel.isEmpty()) {
				if(commandParallel.equals("true")) {
					commandConfig.setParallelExec(true);
				} else if(commandParallel.equals("false")) {
					commandConfig.setParallelExec(false);
				}
			}

			config.addRemoteCommandConfig(commandConfig);
		}
	}
}