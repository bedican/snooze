/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.config;

import uk.co.bedican.snooze.*;

public class RemoteCommandConfig
{
	private String name;
	private String exec;

	private boolean parallelExec = true;
	private boolean outputBuffering = false;

	public RemoteCommandConfig(String name, String exec)
	{
		if((name == null) || (exec == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}
		if(exec.isEmpty()) {
			throw new IllegalArgumentException("Missing exec");
		}

		this.name = name;
		this.exec = exec;
	}

	public String getName()
	{
		return this.name;
	}
	public String getExec()
	{
		return this.exec;
	}

	public boolean getParallelExec()
	{
		return this.parallelExec;
	}
	public boolean getOutputBuffering()
	{
		return this.outputBuffering;
	}

	public void setParallelExec(boolean parallelExec)
	{
		this.parallelExec = parallelExec;
	}
	public void setOutputBuffering(boolean outputBuffering)
	{
		this.outputBuffering = outputBuffering;
	}
}