/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.config;

import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.snooze.*;
import java.util.*;
import java.io.*;

public class Config
{
	private String remoteUser;
	private String remotePassword;

	private File localRepoDir;

	private boolean useReleaseBranches;
	private boolean useSymlinks;
	
	private Map<String, List<String>> clusters = new HashMap<String, List<String>>();
	private Map<String, List<String>> deploymentLayouts = new HashMap<String, List<String>>();

	private Map<String, String> servers = new HashMap<String, String>();
	private Map<String, RepositoryConfig> repositories = new HashMap<String, RepositoryConfig>();
	private Map<String, String> deploymentPaths = new HashMap<String, String>();
	private Map<String, String> clusterLayouts = new HashMap<String, String>();

	private Map<String, ProjectConfig> projectConfigs = new HashMap<String, ProjectConfig>();
	private Map<String, RemoteCommandConfig> remoteCommandConfigs = new HashMap<String, RemoteCommandConfig>();

	private Map<String, ScmClient> scmClients = new HashMap<String, ScmClient>();

	private String switchingDirectory;
	private String defaultLayoutDirectory;

	private int idleTime;

	public Config() throws ConfigException
	{
		this.localRepoDir = new File(System.getProperty("java.io.tmpdir"), "repo");
		this.remoteUser = System.getProperty("user.name");
		this.remotePassword = null;
		this.useReleaseBranches = true;
		this.useSymlinks = false;
		this.switchingDirectory = "spare";
		this.defaultLayoutDirectory = "active";
		this.idleTime = 0;
	}
	public String getRemoteUser()
	{
		return this.remoteUser;
	}
	public String getRemotePassword()
	{
		return this.remotePassword;
	}
	public boolean getUseReleaseBranches()
	{
		return this.useReleaseBranches;
	}
	public boolean getUseSymlinks()
	{
		return this.useSymlinks;
	}
	public File getLocalRepositoriesDirectory()
	{
		return this.localRepoDir;
	}
	public boolean isProject(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		return this.projectConfigs.containsKey(project.toLowerCase());
	}
	public List<String> getProjects()
	{
		return new Vector<String>(this.projectConfigs.keySet());
	}
	public ScmClient getScmClient(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}
		
		project = project.toLowerCase();
		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		if(this.scmClients.containsKey(project)) {
			return this.scmClients.get(project);
		}

		ProjectConfig projectConfig = this.projectConfigs.get(project);

		if(!this.repositories.containsKey(projectConfig.getRepositoryName())) {
			throw new IllegalArgumentException("Unknown repository name " + projectConfig.getRepositoryName());
		}
	
		RepositoryConfig repoConfig = this.repositories.get(projectConfig.getRepositoryName());

		String repository = this.getRepositoryPath(project);
		String mainBranch = this.getRepositoryMainBranch(project);
		
		ScmClient client = repoConfig.getType().newClient(repository, mainBranch, this.localRepoDir);

		this.scmClients.put(project, client);

		return client;
	}
	
	
	public List<String> getServers(String project, String environment)
	{
		if((project == null) || (environment == null)) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}
		if(environment.isEmpty()) {
			throw new IllegalArgumentException("Missing environment");
		}

		project = project.toLowerCase();
		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		String clusterName = this.projectConfigs.get(project).getEnvironment(environment);
		if(clusterName == null) {
			throw new IllegalArgumentException("Unknown environment for project " + project);
		}

		return this.clusters.get(clusterName);
	}
	public List<String> getHosts(String project, String environment)
	{
		List<String> hosts = new Vector<String>();
		List<String> servers = this.getServers(project, environment);

		for(String server:servers) {
			hosts.add(this.servers.get(server));
		}

		return hosts;
	}
	public String getHost(String server)
	{
		if(server == null) {
			throw new NullPointerException();
		}
		if(server.isEmpty()) {
			throw new IllegalArgumentException("Missing server");
		}

		server = server.toLowerCase();

		if(!this.servers.containsKey(server)) {
			throw new IllegalArgumentException("Unknown server");
		}

		return this.servers.get(server);
	}
	public String getDeploymentPath(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		ProjectConfig projectConfig = this.projectConfigs.get(project);
		String deploymentBasePath = this.deploymentPaths.get(projectConfig.getDeploymentName());
		String deploymentPath = projectConfig.getDeploymentPath();

		if(!deploymentPath.isEmpty()) {
			return deploymentBasePath + deploymentPath;
		}

		return deploymentBasePath + "/" + project;
	}
	public String getRepositoryPath(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		ProjectConfig projectConfig = this.projectConfigs.get(project);

		if(!this.repositories.containsKey(projectConfig.getRepositoryName())) {
			throw new IllegalArgumentException("Unknown repository name " + projectConfig.getRepositoryName());
		}

		RepositoryConfig repoConfig = this.repositories.get(projectConfig.getRepositoryName());

		if(!repoConfig.hasBasePath()) {
			return repoConfig.getPath();
		}

		String repositoryPath = projectConfig.getRepositoryPath();
		if(!repositoryPath.isEmpty()) {
			return repoConfig.getBase() + repositoryPath;
		}
		
		return repoConfig.getBase() + "/" + project;
	}
	public String getRepositoryMainBranch(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		ProjectConfig projectConfig = this.projectConfigs.get(project);

		if(!this.repositories.containsKey(projectConfig.getRepositoryName())) {
			throw new IllegalArgumentException("Unknown repository name " + projectConfig.getRepositoryName());
		}
	
		return this.repositories.get(projectConfig.getRepositoryName()).getBranch();
	}
	public List<String> getRemoteCommandNames()
	{
		return new Vector<String>(this.remoteCommandConfigs.keySet());
	}
	public RemoteCommandConfig getRemoteCommandConfig(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		name = name.toLowerCase();

		if(!this.remoteCommandConfigs.containsKey(name)) {
			throw new IllegalArgumentException("Unknown command");
		}

		return this.remoteCommandConfigs.get(name);
	}

	public String getPreDeployExec(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getPreDeployExec();
	}
	public String getPostDeployExec(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getPostDeployExec();
	}
	public String getPreSwitchExec(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getPreSwitchExec();
	}
	public String getPostSwitchExec(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getPostSwitchExec();
	}	
	
	public String getSinglePreDeployExec(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getSinglePreDeployExec();
	}
	public String getSinglePostDeployExec(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getSinglePostDeployExec();
	}
	public String getSinglePreSwitchExec(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getSinglePreSwitchExec();
	}
	public String getSinglePostSwitchExec(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getSinglePostSwitchExec();
	}
	public String getPostExportExec(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getPostExportExec();
	}
	public String getSinglePostExportExec(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getSinglePostExportExec();
	}
	
	public List<String> getExternals(String project)
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}

		project = project.toLowerCase();

		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.projectConfigs.get(project).getExternals();
	}

	public String getSwitchingDirectory()
	{
		return this.switchingDirectory;
	}
	public boolean isDeploymentDirectory(String project, String environment, String directory)
	{
		if((project == null) || (environment == null) || (directory == null)) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}
		if(environment.isEmpty()) {
			throw new IllegalArgumentException("Missing environment");
		}
		if(directory.isEmpty()) {
			throw new IllegalArgumentException("Missing directory");
		}

		project = project.toLowerCase();
		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		String clusterName = this.projectConfigs.get(project).getEnvironment(environment);
		if(clusterName == null) {
			throw new IllegalArgumentException("Unknown environment for project " + project);
		}

		String clusterLayout = this.clusterLayouts.get(clusterName);
		if(clusterLayout == null) {
			return directory.equals(this.defaultLayoutDirectory);
		}

		return this.deploymentLayouts.get(clusterLayout).contains(directory);
	}
	public List<String> getDeploymentDirectories(String project, String environment)
	{
		if((project == null) || (environment == null)) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}
		if(environment.isEmpty()) {
			throw new IllegalArgumentException("Missing environment");
		}
		
		project = project.toLowerCase();
		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		String clusterName = this.projectConfigs.get(project).getEnvironment(environment);
		if(clusterName == null) {
			throw new IllegalArgumentException("Unknown environment for project " + project);
		}
		
		List<String> directories = new Vector<String>();
		directories.add(this.defaultLayoutDirectory);

		String clusterLayout = this.clusterLayouts.get(clusterName);
		if(clusterLayout == null) {
			return directories;
		}
		
		return Collections.unmodifiableList(this.deploymentLayouts.get(clusterLayout));
	}
	public String getDefaultDeploymentDirectory(String project, String environment)
	{
		if((project == null) || (environment == null)) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}
		if(environment.isEmpty()) {
			throw new IllegalArgumentException("Missing environment");
		}

		project = project.toLowerCase();
		if(!this.projectConfigs.containsKey(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		String clusterName = this.projectConfigs.get(project).getEnvironment(environment);
		if(clusterName == null) {
			throw new IllegalArgumentException("Unknown environment for project " + project);
		}

		String clusterLayout = this.clusterLayouts.get(clusterName);
		if(clusterLayout == null) {
			return this.defaultLayoutDirectory;
		}

		return this.deploymentLayouts.get(clusterLayout).get(0);
	}

	public int getIdleTime()
	{
		return this.idleTime;
	}

	public void setRemoteUser(String remoteUser)
	{
		if(remoteUser == null) {
			throw new NullPointerException();
		}
		if(remoteUser.isEmpty()) {
			throw new IllegalArgumentException("Missing remote user");
		}

		this.remoteUser = remoteUser;
	}
	public void setRemotePassword(String remotePassword)
	{
		if(remotePassword == null) {
			throw new NullPointerException();
		}
		if(remotePassword.isEmpty()) {
			throw new IllegalArgumentException("Missing remote password");
		}

		this.remotePassword = remotePassword;
	}

	public void setUseReleaseBranches(boolean useReleaseBranches)
	{
		this.useReleaseBranches = useReleaseBranches;
	}
	
	public void setUseSymlinks(boolean useSymlinks)
	{
		this.useSymlinks = useSymlinks;
	}

	public void setLocalRepositoriesDirectory(File localRepoDir)
	{
		if(localRepoDir == null) {
			throw new NullPointerException();
		}
		if(!localRepoDir.isAbsolute()) {
			throw new IllegalArgumentException("localRepoDir is not absolute");
		}

		this.localRepoDir = localRepoDir;
	}
	
	public void addServer(String name, String host)
	{
		if((name == null) || (host == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}
		if(host.isEmpty()) {
			throw new IllegalArgumentException("Missing host");
		}

		this.servers.put(name.toLowerCase(), host);
	}
	public void addServerToCluster(String name, String serverName)
	{
		if((name == null) || (serverName == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing cluster name");
		}
		if(serverName.isEmpty()) {
			throw new IllegalArgumentException("Missing server name");
		}

		name = name.toLowerCase();
		serverName = serverName.toLowerCase();

		if(!this.servers.containsKey(serverName)) {
			throw new IllegalArgumentException("Unknown server name " + serverName);
		}

		List<String> servers;

		if(this.clusters.containsKey(name)) {
			servers = this.clusters.get(name);
		} else {
			servers = new Vector<String>();
		}

		if(!servers.contains(serverName)) {
			servers.add(serverName);
		}

		this.clusters.put(name, servers);
	}
	public void setClusterLayout(String name, String layoutName)
	{
		if((name == null) || (layoutName == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing cluster name");
		}
		if(layoutName.isEmpty()) {
			throw new IllegalArgumentException("Missing layout");
		}

		name = name.toLowerCase();
		layoutName = layoutName.toLowerCase();

		if(!this.deploymentLayouts.containsKey(layoutName)) {
			throw new IllegalArgumentException("Unknown layout name " + layoutName);
		}

		this.clusterLayouts.put(name, layoutName);
	}
	public void addRepositoryConfig(RepositoryConfig config)
	{
		if(config == null) {
			throw new NullPointerException();
		}

		this.repositories.put(config.getName().toLowerCase(), config);
	}
	public void addDeploymentPath(String name, String path)
	{
		if((name == null) || (path == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}
		if(path.isEmpty()) {
			throw new IllegalArgumentException("Missing path");
		}

		this.deploymentPaths.put(name.toLowerCase(), path);
	}
	public void addProjectConfig(ProjectConfig config) throws ConfigException
	{
		if(config == null) {
			throw new NullPointerException();
		}

		// The repository name must exist
		if(!this.repositories.containsKey(config.getRepositoryName())) {
			throw new ConfigException("Project " + config.getName() + " contains an invalid repository name " + config.getRepositoryName());
		}

		// The deployment path name must exist
		if(!this.deploymentPaths.containsKey(config.getDeploymentName())) {
			throw new ConfigException("Project " + config.getName() + " contains an invalid deployment path name " + config.getDeploymentName());
		}

		// The cluster names must exist
		for(String clusterName:config.getEnvironments().values()) {
			if(!this.clusters.containsKey(clusterName)) {
				throw new ConfigException("Project " + config.getName() + " contains an invalid cluster name " + clusterName);
			}
		}

		this.projectConfigs.put(config.getName().toLowerCase(), config);
	}
	public void addRemoteCommandConfig(RemoteCommandConfig config)
	{
		if(config == null) {
			throw new NullPointerException();
		}

		this.remoteCommandConfigs.put(config.getName().toLowerCase(), config);
	}
	public void addDeploymentLayout(String name, List<String> directories)
	{
		if((name == null) || (directories == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}
		if(directories.isEmpty()) {
			throw new IllegalArgumentException("Missing directories");
		}

		name = name.toLowerCase();

		if(this.deploymentLayouts.containsKey(name)) {
			return;
		}

		this.deploymentLayouts.put(name, directories);
	}

	public void setIdleTime(int idleTime)
	{
		if(idleTime == 0) {
			throw new IllegalArgumentException("Zero idle time");
		}

		this.idleTime = idleTime;
	}
}