/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze;

import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.tasks.*;
import uk.co.bedican.squirrel.*;
import java.util.*;
import java.net.*;

public class SnoozeApplication extends AbstractApplication
{
	public static final String VERSION = "Snooze " + SnoozeApplication.class.getPackage().getImplementationVersion();

	private Config config;

	public SnoozeApplication()
	{
	}

	protected void configure() throws ApplicationException
	{
		ConfigBuilder builder = null;

		try {
			builder = new ConfigBuilder();
			this.config = builder.build("etc/conf.xml");
		} catch(ConfigException ce) {
			throw new ApplicationException(ce);
		}
		
		this.addTask("projects",	new ListProjectsTask());
		
		if(this.config.getUseReleaseBranches()) {
			this.addTask("branch", new BranchTask());
			this.addTask("tag", new TagBranchTask());
			this.addTask("branches", new ListBranchesTask());
			this.addTask("mybranches", 	new ListMyBranchesTask());
		} else {
			this.addTask("tag", new TagMainBranchTask());
		}

		this.addTask("tags", new ListTagsTask());
		
		if(this.config.getUseSymlinks()) {
			this.addTask("export", 	new ExportTask());
			this.addTask("rm-export", new CleanExportTask());
			this.addTask("deploy", new DeploySymlinkTask());
		} else {
			this.addTask("switch", new SwitchTask());
			this.addTask("deploy", new DeployMoveTask());
		}
		
		this.addTask("query", new QueryTask());
		this.addTask("info", new InfoTask());
		this.addTask("unlock", new UnlockTask());
		this.addTask("about", new AboutTask());

		List<String> commands = this.config.getRemoteCommandNames();

		for(String command:commands) {
			this.addTask(command, new RemoteCommandTask(this.config.getRemoteCommandConfig(command)));
		}
	}

	public Config getConfig()
	{
		return this.config;
	}

	public static void main(String[] args)
	{
		ApplicationLoader.run(SnoozeApplication.class, args);
	}
}