/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class SwitchTask extends SnoozeTask
{
	public SwitchTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Switches the spare directory to the specified tag or branch version");
		this.setHelp("\n  Usage:\n\t%name% <project> <environment> <type> <version> [checkout]\n\n  Switches the spare directory to the specified tag or branch version.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// switch <project> <environment> <type> <version> [checkout]

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		} else if(args.size() < 3) {
			out.println("Please specify an environment");
			return;
		} else if(args.size() < 4) {
			out.println("Please specify a version type");
			return;
		} else if(args.size() < 5) {
			out.println("Please specify a version");
			return;
		} else if((args.size() == 6) && (!args.get(5).equals("checkout"))) {
			out.println("Unknown argument: " + args.get(5));
			return;
		} else if(args.size() > 6) {
			out.println("Too many arguments");
			return;
		}

		String project = args.get(1);
		String environment = args.get(2);
		String type = args.get(3);
		String version = args.get(4);

		boolean forceCheckout = (args.size() == 6);
		
		Config config = this.getApplication().getConfig();
		
		if(config.getUseReleaseBranches())
		{
			if((!type.equals("branch")) && (!type.equals("tag")) && (!type.equals("mybranch"))) {
				out.println("Version type must be either tag, branch or mybranch");
				return;
			}
		}
		else
		{
			if(!type.equals("tag")) {
				out.println("Invalid version type");
				return;
			}
		}

		if(!this.lockProject(project)) {
			out.println("Failed to obtain project lock.");
			return;
		}

		try
		{
			String deployment = config.getDeploymentPath(project);
			String switching = config.getSwitchingDirectory();
			
			String directory = deployment + "/" + switching;

			ScmClient scm = config.getScmClient(project);

			if(type.equals("tag"))
			{
				if(!scm.isTagValid(version)) {
					out.println("Tag is not a valid format");
					return;
				} else if(!scm.isTag(version)) {
					out.println("Unknown tag version");
					return;
				}
			}
			else if(type.equals("branch"))
			{
				if(!scm.isBranchValid(version)) {
					out.println("Branch is not a valid format");
					return;
				} else if(!scm.isBranch(version)) {
					out.println("Unknown release branch version");
					return;
				}
			}
			else if(type.equals("mybranch"))
			{
				if(!scm.isMyBranchValid(version)) {
					out.println("MyBranch is not a valid format");
					return;
				} else if(!scm.isMyBranch(version)) {
					out.println("Unknown branch");
					return;
				}
			}

			DistributedShellExec distExec = new DistributedShellExec();

			distExec.setUser(config.getRemoteUser());
			distExec.setPassword(config.getRemotePassword());
			
			String singlePreSwitchExec = config.getSinglePreSwitchExec(project);			
			
			if(!singlePreSwitchExec.isEmpty())
			{
				ShellExecCommand command;
				if(singlePreSwitchExec.startsWith("/")) {
					command = new ShellExecCommand(singlePreSwitchExec);
				} else {
					command = new ShellExecCommand("if [ -d " + directory + " ]; then " + directory + "/" + singlePreSwitchExec + " ; fi");
				}
				
				command.setSingleServerMode(true);
				distExec.addCommand(command);
			}

			String preSwitchExec = config.getPreSwitchExec(project);

			if(!preSwitchExec.isEmpty())
			{
				if(preSwitchExec.startsWith("/")) {
					distExec.addCommand(new ShellExecCommand(preSwitchExec));
				} else {
					distExec.addCommand(new ShellExecCommand("if [ -d " + directory + " ]; then " + directory + "/" + preSwitchExec + " ; fi"));
				}
			}			
			
			scm.addRemoteSwitchingCommands(distExec, type, version, directory, forceCheckout);

			// Drop a version file, so QueryTask can use it.
			distExec.addCommand(new ShellExecCommand("echo '" + type + " " + version + "' > " + directory + "/version"));
			
			String singlePostSwitchExec = config.getSinglePostSwitchExec(project);		
			
			if(!singlePostSwitchExec.isEmpty()) {
				ShellExecCommand command = new ShellExecCommand(singlePostSwitchExec.startsWith("/") ? singlePostSwitchExec : (directory + "/" + singlePostSwitchExec), false);
				command.setSingleServerMode(true);
				distExec.addCommand(command);
			}		

			String postSwitchExec = config.getPostSwitchExec(project);

			if(!postSwitchExec.isEmpty()) {
				distExec.addCommand(new ShellExecCommand(
					postSwitchExec.startsWith("/") ? postSwitchExec : (directory + "/" + postSwitchExec), false
				));
			}

			List<String> servers = config.getServers(project, environment);

			for(String server:servers) {
				distExec.addHost(server, config.getHost(server));
			}

			try {
				distExec.execute(out);
			} catch(Exception e) {
				throw new TaskException(e);
			}
		}
		catch(ScmClientException sce)
		{
			throw new TaskException(sce);
		}
		finally
		{
			if(!this.unlockProject(project)) {
				out.println("Failed to unlock project, use the unlock command to cleanup.");
			}
		}
	}
}