/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class AboutTask extends SnoozeTask
{
	public AboutTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Displays about information");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		PrintStream out = this.getPrintStream();

		out.println();
		out.println(" " + SnoozeApplication.VERSION);
		out.println(" http://www.bedican.co.uk");
		out.println(" (c) Copyright 2010 Bedican Solutions");
		out.println();
	}
}