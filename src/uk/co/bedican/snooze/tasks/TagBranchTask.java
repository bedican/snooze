/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class TagBranchTask extends SnoozeTask
{
	public TagBranchTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Creates a tag within the source code repository");
		this.setHelp("\n  Usage:\n\t%name% <project> [version]\n\n  Creates a tag within the source code repository. If the version is not specified, the current release branch version will be used.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// tag <project> [version]

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		} else if(args.size() > 3) {
			out.println("Too many arguments");
			return;
		}

		String project = args.get(1);

		if(!this.lockProject(project)) {
			out.println("Failed to obtain project lock.");
			return;
		}

		try
		{
			Config config = this.getApplication().getConfig();
			
			String version;
			String repository = config.getRepositoryPath(project);
			String mainBranch = config.getRepositoryMainBranch(project);

			ScmClient scm = config.getScmClient(project);

			if(args.size() == 3) {
				version = args.get(2);
			} else {
				version = scm.getCurrentBranch();
			}

			if(!scm.isBranchValid(version)) {
				out.println("Branch " + version + " is not a valid format.");
				return;
			}

			if(!scm.isBranch(version)) {
				out.println("Branch " + version + " does not exists.");
				return;
			}

			String tag = scm.createTagFromBranch(version);

			out.println("Created tag " + tag + " from branch " + version + " for " + project);
		}
		catch(ScmClientException sce)
		{
			throw new TaskException(sce);
		}
		finally
		{
			if(!this.unlockProject(project)) {
				out.println("Failed to unlock project, use the unlock command to cleanup.");
			}
		}
	}
}