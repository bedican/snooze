/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.squirrel.*;
import java.io.*;

public abstract class SnoozeTask extends AbstractTask
{
	private static File tmpDir = new File("tmp");

	public SnoozeApplication getApplication()
	{
		return (SnoozeApplication)super.getApplication();
	}

	private File getProjectLockFile(String project)
	{
		return new File(tmpDir, project + ".lock");
	}

	protected boolean lockProject(String project) throws IOException
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}
		if(!this.getApplication().getConfig().isProject(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		// A very simplistic lock mechanism. 
		// While createNewFile is atomic it does not actually create a lock on the file, but is sufficient for our needs.

		return this.getProjectLockFile(project).createNewFile();
	}

	protected boolean unlockProject(String project) throws IOException
	{
		if(project == null) {
			throw new NullPointerException();
		}
		if(project.isEmpty()) {
			throw new IllegalArgumentException("Missing project");
		}
		if(!this.getApplication().getConfig().isProject(project)) {
			throw new IllegalArgumentException("Unknown project");
		}

		return this.getProjectLockFile(project).delete();
	}
}