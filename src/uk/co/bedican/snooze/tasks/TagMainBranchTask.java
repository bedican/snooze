/**
 * (c) Copyright 2011 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class TagMainBranchTask extends SnoozeTask
{
	public TagMainBranchTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Creates a tag within the source code repository");
		this.setHelp("\n  Usage:\n\t%name% <project> [version]\n\n  Creates a tag within the source code repository. If the version is not specified, the next tag version will be used.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// tag <project> [version]

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		} else if(args.size() > 3) {
			out.println("Too many arguments");
			return;
		}

		String project = args.get(1);

		if(!this.lockProject(project)) {
			out.println("Failed to obtain project lock.");
			return;
		}

		try
		{
			Config config = this.getApplication().getConfig();
			
			String version;

			ScmClient scm = config.getScmClient(project);

			if(args.size() == 3) {
				version = args.get(2);
			} else {
				version = scm.getNextTag();
			}

			if(!scm.isVersionValid(version)) {
				out.println("Tag " + version + " is not a valid format.");
				return;
			}

			if(scm.isTag(version)) {
				out.println("Tag " + version + " already exists.");
				return;
			}

			String mainBranch = config.getRepositoryMainBranch(project);
			String tag = scm.createTagFromMainBranch(version);

			out.println("Created tag " + tag + " from " + mainBranch + " for " + project);
			
			List<String> externals = config.getExternals(project);
			if(!externals.isEmpty())
			{
				Integer revision = scm.getTagLastChangedRevision(tag);

				for(String external:externals) {
					out.print("Fixing externals on '" + external + "' to revision " + revision.toString() + "... ");
					scm.fixTagExternalToRevision(version, external, revision);
					out.println("Done.");
				}
			}
		}
		catch(ScmClientException sce)
		{
			throw new TaskException(sce);
		}
		finally
		{
			if(!this.unlockProject(project)) {
				out.println("Failed to unlock project, use the unlock command to cleanup.");
			}
		}
	}
}