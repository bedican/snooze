/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class RemoteCommandTask extends SnoozeTask
{
	private RemoteCommandConfig config;

	public RemoteCommandTask(RemoteCommandConfig config)
	{
		if(config == null) {
			throw new NullPointerException();
		}

		this.config = config;
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Executes '" + this.config.getExec() + "' remotely");
		this.setHelp("\n  Usage:\n\t%name% <project> <environment> \n\n  Executes '" + this.config.getExec() + "' remotely.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// command <project> <environment>

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		} else if(args.size() < 3) {
			out.println("Please specify an environment");
			return;
		} else if(args.size() > 3) {
			out.println("Too many arguments");
			return;
		}

		String project = args.get(1);

		if(!this.lockProject(project)) {
			out.println("Failed to obtain project lock.");
			return;
		}

		try
		{
			Config config = this.getApplication().getConfig();

			String environment = args.get(2);

			ShellExecCommand command = new ShellExecCommand(this.config.getExec());
			command.setOutputBuffering(this.config.getOutputBuffering());

			DistributedShellExec distExec = new DistributedShellExec(command);
			distExec.setParallelExec(this.config.getParallelExec());

			distExec.setUser(config.getRemoteUser());
			distExec.setPassword(config.getRemotePassword());

			List<String> servers = config.getServers(project, environment);

			for(String server:servers) {
				distExec.addHost(server, config.getHost(server));
			}

			try {
				distExec.execute(out);
			} catch(Exception e) {
				throw new TaskException(e);
			}
		}
		finally
		{
			if(!this.unlockProject(project)) {
				out.println("Failed to unlock project, use the unlock command to cleanup.");
			}
		}
	}
}