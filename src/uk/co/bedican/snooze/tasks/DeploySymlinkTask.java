/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class DeploySymlinkTask extends SnoozeTask
{
	public DeploySymlinkTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Deploys the specified directory");
		this.setHelp("\n  Usage:\n\t%name% <project> <environment> <type> <version> [directory]\n\n  Deploys the given directory to a specified version. If the deployment directory is not specified, the default deployment directory will be used.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// deploy <project> <environment> <type> <version> [directory]

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		} else if(args.size() < 3) {
			out.println("Please specify an environment");
			return;
		} else if(args.size() < 4) {
			out.println("Please specify a version type");
			return;
		} else if(args.size() < 5) {
			out.println("Please specify a version");
			return;
		} else if(args.size() > 6) {
			out.println("Too many arguments");
			return;
		}
		
		Config config = this.getApplication().getConfig();

		String directory;
		String project = args.get(1);
		String environment = args.get(2);
		String type = args.get(3);
		String version = args.get(4);
		
		if(config.getUseReleaseBranches())
		{
			if((!type.equals("branch")) && (!type.equals("tag")) && (!type.equals("mybranch"))) {
				out.println("Version type must be either tag, branch or mybranch");
				return;
			}
		}
		else
		{
			if(!type.equals("tag")) {
				out.println("Invalid version type");
				return;
			}
		}

		if(args.size() == 6)
		{
			directory = args.get(5);
			if(!config.isDeploymentDirectory(project, environment, directory)) {
				out.println("Invalid deployment directory");
				return;
			}
		}
		else
		{
			directory = config.getDefaultDeploymentDirectory(project, environment);
		}

		if(!this.lockProject(project)) {
			out.println("Failed to obtain project lock.");
			return;
		}

		try
		{
			String deployment = config.getDeploymentPath(project);

			String checkout = deployment + "/" + type + "/" + version;
			String symlink = deployment + "/" + directory;
			
			DistributedShellExec distExec = new DistributedShellExec();

			distExec.setUser(config.getRemoteUser());
			distExec.setPassword(config.getRemotePassword());
			
			String singlePreDeployExec = config.getSinglePreDeployExec(project);		
			
			if(!singlePreDeployExec.isEmpty())
			{
				ShellExecCommand command;
				if(singlePreDeployExec.startsWith("/")) {
					command = new ShellExecCommand(singlePreDeployExec);
				} else {
					command = new ShellExecCommand("if [ -d " + symlink + " ]; then " + symlink + "/" + singlePreDeployExec + " ; fi");
				}
				
				command.setSingleServerMode(true);
				distExec.addCommand(command);
			}

			String preDeployExec = config.getPreDeployExec(project);

			if(!preDeployExec.isEmpty())
			{
				if(preDeployExec.startsWith("/")) {
					distExec.addCommand(new ShellExecCommand(preDeployExec));
				} else {
					distExec.addCommand(new ShellExecCommand("if [ -d " + symlink + " ]; then " + symlink + "/" + preDeployExec + " ; fi"));
				}
			}

			distExec.addCommand(new ShellExecCommand(
				"if [ -L " + symlink + " ]; then unlink " + symlink + " ; fi"
			));

			distExec.addCommand(new ShellExecCommand(
				"ln -s " + checkout + " " + symlink
			));
			
			String singlePostDeployExec = config.getSinglePostDeployExec(project);

			if(!singlePostDeployExec.isEmpty()) {
				ShellExecCommand command = new ShellExecCommand(singlePostDeployExec.startsWith("/") ? singlePostDeployExec : (symlink + "/" + singlePostDeployExec) , false);
				command.setSingleServerMode(true);
				distExec.addCommand(command);
			}

			String postDeployExec = config.getPostDeployExec(project);

			if(!postDeployExec.isEmpty()) {
				distExec.addCommand(new ShellExecCommand(
					postDeployExec.startsWith("/") ? postDeployExec : (symlink + "/" + postDeployExec) , false
				));
			}

			List<String> servers = config.getServers(project, environment);

			for(String server:servers) {
				distExec.addHost(server, config.getHost(server));
			}

			try {
				distExec.execute(out);
			} catch(Exception e) {
				throw new TaskException(e);
			}
		}
		finally
		{
			if(!this.unlockProject(project)) {
				out.println("Failed to unlock project, use the unlock command to cleanup.");
			}
		}
	}
}