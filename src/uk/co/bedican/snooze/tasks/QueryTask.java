/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class QueryTask extends SnoozeTask
{
	public QueryTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Queries the deployment directories and displays their current version");
		this.setHelp("\n  Usage:\n\t%name% <project> <environment> [directory]\n\n  Queries the deployment directories for their current version.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// query <project> <environment> [directory]

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		} else if(args.size() < 3) {
			out.println("Please specify an environment");
			return;
		} else if(args.size() > 4) {
			out.println("Too many arguments");
			return;
		}

		String project = args.get(1);

		if(!this.lockProject(project)) {
			out.println("Failed to obtain project lock.");
			return;
		}

		try
		{
			Config config = this.getApplication().getConfig();

			List<String> directories;
			String environment = args.get(2);
			ScmClient scm = config.getScmClient(project);

			if(args.size() == 4)
			{
				String directory = args.get(3);
				if(!config.isDeploymentDirectory(project, environment, directory)) {
					out.println("Invalid deployment directory");
					return;
				}
				
				directories = new Vector<String>();
				directories.add(directory);
			}
			else
			{
				directories = config.getDeploymentDirectories(project, environment);
			}
			
			if(!config.getUseSymlinks()) {
				directories.add(config.getSwitchingDirectory());
			}

			DistributedShellExec distExec = new DistributedShellExec();
			distExec.setParallelExec(false);

			distExec.setUser(config.getRemoteUser());
			distExec.setPassword(config.getRemotePassword());

			String deployment = config.getDeploymentPath(project);
			
			if(config.getUseSymlinks()) {
				
				StringBuffer buffer = new StringBuffer();
				buffer.append("file ");
				
				for(String directory:directories) {
					buffer.append(" ").append(deployment).append("/").append(directory);
				}
				
				distExec.addCommand(new ShellExecCommand(buffer.toString(), false));

			} else {
				for(String directory:directories) {
					distExec.addCommand(new ShellExecCommand("cat " + deployment + "/" + directory + "/version" , false));
				}
			}

			List<String> servers = config.getServers(project, environment);

			for(String server:servers) {
				distExec.addHost(server, config.getHost(server));
			}

			try {
				distExec.execute(out);
			} catch(Exception e) {
				throw new TaskException(e);
			}
		}
		finally
		{
			if(!this.unlockProject(project)) {
				out.println("Failed to unlock project, use the unlock command to cleanup.");
			}
		}
	}
}