/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class ListTagsTask extends SnoozeTask
{
	public ListTagsTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Lists the available tags for a given project");
		this.setHelp("\n  Usage:\n\t%name% <project>\n\n  Lists the available tags for a given project.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		try  {
			this.process(args);
		} catch(ScmClientException sce) {
			throw new TaskException(sce);
		}
	}

	private void process(List<String> args) throws ScmClientException, IOException
	{
		// tags <project>

		PrintStream out = this.getPrintStream();

		if(args.size() != 2) {
			out.println("Please specify a project");
			return;
		}
		
		Config config = this.getApplication().getConfig();

		String project = args.get(1);

		ScmClient scm = config.getScmClient(project);
		List<String> versions = scm.getTags();

		for(String version:versions) {
			out.println(version);
		}
	}
}