/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class ListProjectsTask extends SnoozeTask
{
	public ListProjectsTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Displays list of available projects");
		this.setHelp("\n  Usage:\n\t%name%\n\n  Displays list of available projects.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		PrintStream out = this.getPrintStream();

		if(args.size() >1) {
			out.println("Too many arguments");
			return;
		}

		List<String> projects = this.getApplication().getConfig().getProjects();

		for(String project:projects) {
			out.println(project);
		}
	}
}