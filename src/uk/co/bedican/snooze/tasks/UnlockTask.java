/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class UnlockTask extends SnoozeTask
{
	public UnlockTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Removes a lock for a given project");
		this.setHelp("\n  Usage:\n\t%name% <project>\n\n  Removes a lock for a given project.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// unlock <project>

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		}

		String project = args.get(1);

		if(!this.unlockProject(project)) {
			out.println("Failed to unlock project, try manual deletion.");
		}
	}
}