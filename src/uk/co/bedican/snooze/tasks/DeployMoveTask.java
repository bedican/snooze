/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class DeployMoveTask extends SnoozeTask
{
	public DeployMoveTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Deploys the spare directory");
		this.setHelp("\n  Usage:\n\t%name% <project> <environment> [directory]\n\n  Deploys the current spare directory to the deployment directory. If the deployment directory is not specified, the default deployment directory will be used.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// deploy <project> <environment> [directory]

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		} else if(args.size() < 3) {
			out.println("Please specify an environment");
			return;
		} else if(args.size() > 4) {
			out.println("Too many arguments");
			return;
		}

		String project = args.get(1);

		if(!this.lockProject(project)) {
			out.println("Failed to obtain project lock.");
			return;
		}

		try
		{
			Config config = this.getApplication().getConfig();

			String directory;
			String environment = args.get(2);

			if(args.size() == 4)
			{
				directory = args.get(3);
				if(!config.isDeploymentDirectory(project, environment, directory)) {
					out.println("Invalid deployment directory");
					return;
				}
			}
			else
			{
				directory = config.getDefaultDeploymentDirectory(project, environment);
			}

			String switching = config.getSwitchingDirectory();
			String deployment = config.getDeploymentPath(project);

			DistributedShellExec distExec = new DistributedShellExec();

			distExec.setUser(config.getRemoteUser());
			distExec.setPassword(config.getRemotePassword());
			
			String singlePreDeployExec = config.getSinglePreDeployExec(project);			
			
			if(!singlePreDeployExec.isEmpty())
			{
				ShellExecCommand command;
				if(singlePreDeployExec.startsWith("/")) {
					command = new ShellExecCommand(singlePreDeployExec);
				} else {
					command = new ShellExecCommand("if [ -d " + deployment + "/" + directory + " ]; then " + deployment + "/" + directory + "/" + singlePreDeployExec + " ; fi");
				}
				
				command.setSingleServerMode(true);
				distExec.addCommand(command);
			}

			String preDeployExec = config.getPreDeployExec(project);

			if(!preDeployExec.isEmpty())
			{
				if(preDeployExec.startsWith("/")) {
					distExec.addCommand(new ShellExecCommand(preDeployExec));
				} else {
					distExec.addCommand(new ShellExecCommand("if [ -d " + deployment + "/" + directory + " ]; then " + deployment + "/" + directory + "/" + preDeployExec + " ; fi"));
				}
			}

			directory = deployment + "/" + directory;
			switching = deployment + "/" + switching;

			String tmpDirectory = directory + "-tmp";

			// We wrap the mv command within directory existance checks as it may not always be the case
			// that both the directories exist, for example with a fresh deployment.

			distExec.addCommand(new ShellExecCommand(
				"if [ -d " + directory + " ]; then mv " + directory + " " + tmpDirectory + " ; fi", 
				"if [ -d " + tmpDirectory + " ]; then mv " + tmpDirectory + " " + directory + " ; fi"
			));

			distExec.addCommand(new ShellExecCommand(
				"if [ -d " + switching + " ]; then mv " + switching + " " + directory + " ; fi",
				"if [ -d " + directory + " ]; then mv " + directory + " " + switching + " ; fi"
			));

			distExec.addCommand(new ShellExecCommand(
				"if [ -d " + tmpDirectory + " ]; then mv " + tmpDirectory + " " + switching + " ; fi",
				"if [ -d " + switching + " ]; then mv " + switching + " " + tmpDirectory + " ; fi"
			));
			
			String singlePostDeployExec = config.getSinglePostDeployExec(project);

			if(!singlePostDeployExec.isEmpty()) {
				ShellExecCommand command = new ShellExecCommand(singlePostDeployExec.startsWith("/") ? singlePostDeployExec : (deployment + "/" + directory + "/" + singlePostDeployExec) , false);
				command.setSingleServerMode(true);
				distExec.addCommand(command);
			}

			String postDeployExec = config.getPostDeployExec(project);

			if(!postDeployExec.isEmpty()) {
				distExec.addCommand(new ShellExecCommand(
					postDeployExec.startsWith("/") ? postDeployExec : (deployment + "/" + directory + "/" + postDeployExec) , false
				));
			}

			List<String> servers = config.getServers(project, environment);

			for(String server:servers) {
				distExec.addHost(server, config.getHost(server));
			}

			try {
				distExec.execute(out);
			} catch(Exception e) {
				throw new TaskException(e);
			}
		}
		finally
		{
			if(!this.unlockProject(project)) {
				out.println("Failed to unlock project, use the unlock command to cleanup.");
			}
		}
	}
}