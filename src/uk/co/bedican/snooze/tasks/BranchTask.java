/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class BranchTask extends SnoozeTask
{
	public BranchTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Creates a branch within the source code repository");
		this.setHelp("\n  Usage:\n\t%name% <project> [version]\n\n  Creates a branch within the source code repository. If the version is not specified, the next release branch version will be calculated.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// branch <project> [version]

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		} else if(args.size() > 3) {
			out.println("Too many arguments");
			return;
		}

		String project = args.get(1);

		if(!this.lockProject(project)) {
			out.println("Failed to obtain project lock.");
			return;
		}

		try
		{
			Config config = this.getApplication().getConfig();

			String version;
			ScmClient scm = config.getScmClient(project);

			if(args.size() == 3) {
				version = args.get(2);
				version = (version.startsWith("rc") ? version : ("rc" + version));
			} else {
				version = scm.getNextBranch();
			}

			if(!scm.isBranchValid(version)) {
				out.println("Branch " + version + " is not a valid format.");
				return;
			}

			if(scm.isBranch(version)) {
				out.println("Branch " + version + " already exists.");
				return;
			}

			scm.createBranchFromMainBranch(version);

			out.println("Created branch " + version + " for " + project);

			List<String> externals = config.getExternals(project);
			if(!externals.isEmpty())
			{
				Integer revision = scm.getBranchLastChangedRevision(version);

				for(String external:externals) {
					out.print("Fixing externals on '" + external + "' to revision " + revision.toString() + "... ");
					scm.fixBranchExternalToRevision(version, external, revision);
					out.println("Done.");
				}
			}
		}
		catch(ScmClientException sce)
		{
			throw new TaskException(sce);
		}
		finally
		{
			if(!this.unlockProject(project)) {
				out.println("Failed to unlock project, use the unlock command to cleanup.");
			}
		}
	}
}