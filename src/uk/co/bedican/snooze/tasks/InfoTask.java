/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class InfoTask extends SnoozeTask
{
	public InfoTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Displays configuration information for a given project");
		this.setHelp("\n  Usage:\n\t%name% <project>\n\n  Displays configuration information for a given project.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// info <project>

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		} else if(args.size() > 2) {
			out.println("Too many arguments");
			return;
		}

		Config config = this.getApplication().getConfig();

		String project = args.get(1);

		out.println("Scm path: " + config.getRepositoryPath(project));
		out.println("Scm main branch: " + config.getRepositoryMainBranch(project));
		out.println("Deploy path: " + config.getDeploymentPath(project));

		ScmClient scm = config.getScmClient(project);
		out.println("Local repo dir: " + scm.getRepoDir());
		
		if(config.getUseSymlinks()) {
			String postExportExec = config.getPostExportExec(project);
			if(!postExportExec.isEmpty()) {
				out.println("Post-export exec: " + postExportExec);
			}
			
			String singlePostExportExec = config.getSinglePostExportExec(project);
			if(!singlePostExportExec.isEmpty()) {
				out.println("Single post-export exec: " + singlePostExportExec);
			}
		} else {
			String preSwitchExec = config.getPreSwitchExec(project);
			if(!preSwitchExec.isEmpty()) {
				out.println("Pre-switch exec: " + preSwitchExec);
			}
			
			String postSwitchExec = config.getPostSwitchExec(project);
			if(!postSwitchExec.isEmpty()) {
				out.println("Post-switch exec: " + postSwitchExec);
			}
			
			String singlePreSwitchExec = config.getSinglePreSwitchExec(project);
			if(!singlePreSwitchExec.isEmpty()) {
				out.println("Single pre-switch exec: " + singlePreSwitchExec);
			}
			
			String singlePostSwitchExec = config.getSinglePostSwitchExec(project);
			if(!singlePostSwitchExec.isEmpty()) {
				out.println("Single post-switch exec: " + singlePostSwitchExec);
			}
		}

		String preDeployExec = config.getPreDeployExec(project);
		if(!preDeployExec.isEmpty()) {
			out.println("Pre-deploy exec: " + preDeployExec);
		}

		String postDeployExec = config.getPostDeployExec(project);
		if(!postDeployExec.isEmpty()) {
			out.println("Post-deploy exec: " + postDeployExec);
		}

		String singlePreDeployExec = config.getSinglePreDeployExec(project);
		if(!singlePreDeployExec.isEmpty()) {
			out.println("Single pre-deploy exec: " + singlePreDeployExec);
		}

		String singlePostDeployExec = config.getSinglePostDeployExec(project);
		if(!singlePostDeployExec.isEmpty()) {
			out.println("Single post-deploy exec: " + singlePostDeployExec);
		}
	}
}