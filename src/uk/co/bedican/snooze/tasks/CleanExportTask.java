/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.tasks;

import uk.co.bedican.snooze.*;
import uk.co.bedican.snooze.config.*;
import uk.co.bedican.snooze.scm.*;
import uk.co.bedican.squirrel.*;
import java.io.*;
import java.util.*;

public class CleanExportTask extends SnoozeTask
{
	public CleanExportTask()
	{
	}

	protected void doInit() throws TaskException
	{
		this.setDescription("Cleans up exported tag or branch version");
		this.setHelp("\n  Usage:\n\t%name% <project> <environment> <type> <version>\n\n  Clean up the exported tag or branch version.\n");
	}

	protected void doRun(List<String> args) throws TaskException, IOException
	{
		// clean-export <project> <environment> <type> <version>

		PrintStream out = this.getPrintStream();

		if(args.size() < 2) {
			out.println("Please specify a project");
			return;
		} else if(args.size() < 3) {
			out.println("Please specify an environment");
			return;
		} else if(args.size() < 4) {
			out.println("Please specify a version type");
			return;
		} else if(args.size() < 5) {
			out.println("Please specify a version");
			return;
		} else if(args.size() > 5) {
			out.println("Too many arguments");
			return;
		}

		String project = args.get(1);
		String environment = args.get(2);
		String type = args.get(3);
		String version = args.get(4);
		
		Config config = this.getApplication().getConfig();
		
		if(config.getUseReleaseBranches())
		{
			if((!type.equals("branch")) && (!type.equals("tag")) && (!type.equals("mybranch"))) {
				out.println("Version type must be either tag, branch or mybranch");
				return;
			}
		}
		else
		{
			if(!type.equals("tag")) {
				out.println("Invalid version type");
				return;
			}
		}

		if(!this.lockProject(project)) {
			out.println("Failed to obtain project lock.");
			return;
		}

		String deployment = config.getDeploymentPath(project);
		String directory = type + "/" + version;

		ScmClient scm = config.getScmClient(project);

		if(type.equals("tag"))
		{
			if(!scm.isTagValid(version)) {
				out.println("Tag is not a valid format");
				return;
			}
		}
		else if(type.equals("branch"))
		{
			if(!scm.isBranchValid(version)) {
				out.println("Branch is not a valid format");
				return;
			}
		}
		else if(type.equals("mybranch"))
		{
			if(!scm.isMyBranchValid(version)) {
				out.println("MyBranch is not a valid format");
				return;
			}
		}

		DistributedShellExec distExec = new DistributedShellExec();

		distExec.setUser(config.getRemoteUser());
		distExec.setPassword(config.getRemotePassword());
		
		distExec.addCommand(new ShellExecCommand("if [ -d " + deployment + "/" + directory + " ]; then rm -rf " + deployment + "/" + directory + " ; fi"));

		List<String> servers = config.getServers(project, environment);

		for(String server:servers) {
			distExec.addHost(server, config.getHost(server));
		}

		try {
			distExec.execute(out);
		} catch(Exception e) {
			throw new TaskException(e);
		}

		if(!this.unlockProject(project)) {
			out.println("Failed to unlock project, use the unlock command to cleanup.");
		}
	}
}