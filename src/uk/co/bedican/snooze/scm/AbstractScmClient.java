/**
 * (c) Copyright 2012 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.scm;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.security.*;
import java.math.*;

public abstract class AbstractScmClient implements ScmClient
{
	private static Pattern myBranchPattern = Pattern.compile("(?<!rc)[^\\s]+");
	private static Pattern branchPattern = Pattern.compile("rc([0-9]+)\\.([0-9]+)");
	private static Pattern tagPattern = Pattern.compile("([0-9]+)\\.([0-9]+)(?:-p([0-9]+))?");
	private static Pattern versionPattern = Pattern.compile("([0-9]+)\\.([0-9]+)");

	private Comparator<String> comparator = new VersionComparator();

	private String repository;
	private String mainBranch;
	private File repoDir;
	
	public void init(String repository, String mainBranch, File localDir) throws ScmClientException
	{
		if((repository == null) || (mainBranch == null)) {
			throw new NullPointerException();
		}
		if(repository.isEmpty()) {
			throw new IllegalArgumentException("Missing repository");
		}
		if(mainBranch.isEmpty()) {
			throw new IllegalArgumentException("Missing main branch");
		}
		if(!localDir.isAbsolute()) {
			throw new IllegalArgumentException("localDir is not absolute");
		}

		this.repository = repository;
		this.mainBranch = mainBranch.startsWith("/") ? mainBranch.substring(1) : mainBranch;

		MessageDigest algorithm;

		try {
			algorithm = MessageDigest.getInstance("SHA-1");
		} catch(NoSuchAlgorithmException nsae) {
			throw new ScmClientException("Unknown hashing algorithm", nsae);
		}

		algorithm.update(repository.getBytes());
		byte digest[] = algorithm.digest();

		BigInteger bigInt = new BigInteger(1, digest);
		String hash = bigInt.toString(16);
		while(hash.length() < 32 ) {
		  hash = "0" + hash;
		}

		this.repoDir = new File(localDir, hash);
		this.repoDir.mkdir();
	}
	
	public String getRepository()
	{
		return this.repository;
	}
	
	public String getMainBranch()
	{
		return this.mainBranch;
	}

	public File getRepoDir()
	{
		return this.repoDir;
	}

	protected List<String> runProcess(String... command) throws ScmClientException, IOException
	{
		ProcessBuilder processBuilder = new ProcessBuilder(command);

		if(this.repoDir.exists()) {
			processBuilder.directory(this.repoDir);
		}

		Process process = processBuilder.start();

		BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
		BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream()));

		String line;
		List<String> out = new Vector<String>();

		while((line = in.readLine()) != null)
		{
			out.add(line.trim());
		}

		StringBuffer buffer = new StringBuffer();

		while((line = err.readLine()) != null) {
			buffer.append(line).append("\n");
		}

		try
		{
			if(process.waitFor() != 0) {
				throw new ScmClientException(buffer.toString());
			}
		}
		catch(InterruptedException ie)
		{
			throw new ScmClientException(ie);
		}

		return out;
	}
	
	protected List<String> sortVersions(List<String> versions)
	{
		Collections.sort(versions, this.comparator);
		return versions;
	}
	
	public boolean isBranchValid(String version)
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}

		return this.branchPattern.matcher(version).matches();
	}
	
	public boolean isTagValid(String version)
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}

		return this.tagPattern.matcher(version).matches();
	}

	public boolean isMyBranchValid(String version)
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}

		return this.myBranchPattern.matcher(version).matches();
	}
	
	public boolean isVersionValid(String version)
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}

		return this.versionPattern.matcher(version).matches();
	}
	
	public boolean isBranch(String version) throws ScmClientException, IOException
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}

		return this.getBranches().contains(version);
	}

	public boolean isTag(String version) throws ScmClientException, IOException
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}

		return this.getTags().contains(version);
	}

	public boolean isMyBranch(String branch) throws ScmClientException, IOException
	{
		if(branch == null) {
			throw new NullPointerException();
		}
		if(branch.isEmpty()) {
			throw new IllegalArgumentException("Missing branch");
		}

		return this.getMyBranches().contains(branch);
	}
	
	public String getCurrentBranch() throws ScmClientException, IOException
	{
		List<String> branches = this.getBranches();

		if(branches.isEmpty()) {
			throw new ScmClientException("No branches found");
		}

		return branches.get(branches.size() - 1);
	}

	public String getNextBranch() throws ScmClientException, IOException
	{
		List<String> branches = this.getBranches();

		if(branches.isEmpty()) {
			return "rc1.0";
		}

		try
		{
			String lastVersion = branches.get(branches.size() - 1);

			String[] versionNumbers = lastVersion.split("\\.");
			String major = versionNumbers[0];
			String minor = versionNumbers[1];

			major = major.startsWith("rc") ? major.substring(2) : major;
			major = String.valueOf(Integer.parseInt(major) + 1);
			
			return "rc" + major + "." + minor;

//			String[] versionNumbers = lastVersion.split("\\.");
//			
//			versionNumbers[0] = (versionNumbers[0].startsWith("rc") ? versionNumbers[0] : ("rc" + versionNumbers[0]));
//			versionNumbers[1] = String.valueOf(Integer.parseInt(versionNumbers[1]) + 1);
//
//			return versionNumbers[0] + "." + versionNumbers[1];
		}
		catch(NumberFormatException nfe)
		{
			throw new ScmClientException(nfe);
		}
	}
	
	public String getNextTag(String version) throws ScmClientException, IOException
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(!this.isBranchValid(version)) {
			throw new IllegalArgumentException("Invalid branch version format");
		}

		String tag = version.substring(2); // Remove rc prefix
		List<String> tags = this.getTags();

		if(tags.contains(tag))
		{
			int patch = 1;
			while(tags.contains(tag + "-p" + String.valueOf(patch))) {
				patch++;
			}

			tag = tag + "-p" + String.valueOf(patch);
		}

		return tag;
	}
	
	public String getNextTag() throws ScmClientException, IOException
	{
		List<String> tags = this.getTags();

		if(tags.isEmpty()) {
			return "1.0";
		}
		
		String lastVersion = tags.get(tags.size() - 1);
		
		// Last version may have a patch number, so we use the regex pattern rather than String.split(...) to ignore it
		
		Matcher matcher = this.tagPattern.matcher(lastVersion);
		
		if(!matcher.matches()) {
			throw new ScmClientException("Could not determine next version, invalid format for " + lastVersion);
		}

		try
		{
			return matcher.group(1) + "." + String.valueOf(Integer.parseInt(matcher.group(2)) + 1);
		}
		catch(NumberFormatException nfe)
		{
			throw new ScmClientException(nfe);
		}
	}	

	private static class VersionComparator implements Comparator<String>
	{
		private static Pattern versionPattern = Pattern.compile("(?:rc)?([0-9]+)\\.([0-9]+)(?:-p([0-9]+))?");

		public VersionComparator()
		{
		}

		public int compare(String version1, String version2)
		{
			Matcher matcher1 = this.versionPattern.matcher(version1);
			Matcher matcher2 = this.versionPattern.matcher(version2);

			if((matcher1.matches()) && (!matcher2.matches())) {
				return -1;
			} else if((!matcher1.matches()) && (matcher2.matches())) {
				return 1;
			} else if((!matcher1.matches()) && (!matcher2.matches())) {
				return version1.compareTo(version2);
			}

			int value1, value2;

			try
			{
				for(int x = 1; x < 3; x++)
				{
					value1 = Integer.parseInt(matcher1.group(x));
					value2 = Integer.parseInt(matcher2.group(x));

					if(value1 < value2) {
						return -1;
					} else if(value1 > value2) {
						return 1;
					}
				}

				if((matcher1.groupCount() != 3) && (matcher2.groupCount() == 3)) {
					return -1;
				} else if((matcher1.groupCount() == 3) && (matcher2.groupCount() != 3)) {
					return 1;
				} else if((matcher1.groupCount() != 3) && (matcher2.groupCount() != 3)) {
					return 0;
				}

				value1 = Integer.parseInt(matcher1.group(3));
				value2 = Integer.parseInt(matcher2.group(3));

				if(value1 < value2) {
					return -1;
				} else if(value1 > value2) {
					return 1;
				}
			}
			catch(NumberFormatException nfe)
			{
				// Should never happen...
			}

			return 0;
		}
	}
}