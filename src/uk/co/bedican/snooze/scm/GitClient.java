/**
 * (c) Copyright 2012 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.scm;

import uk.co.bedican.snooze.*;
import java.io.*;
import java.util.*;

public class GitClient extends AbstractScmClient
{
	public void addRemoteSwitchingCommands(DistributedShellExec distExec, String branchType, String version, String directory, boolean forceCheckout)
	{
		if((distExec == null) || (branchType == null) || (version == null) || (directory == null)) {
			throw new NullPointerException();
		}
		if(branchType.isEmpty()) {
			throw new IllegalArgumentException("Missing branch type");
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(directory.isEmpty()) {
			throw new IllegalArgumentException("Missing directory");
		}
		
		if(forceCheckout) {
			distExec.addCommand(new ShellExecCommand("rm -rf " + directory));
			distExec.addCommand(new ShellExecCommand("git clone " + this.getRepository() + " " + directory));
		} else {
			distExec.addCommand(new ShellExecCommand("if [ -d " + directory + " ]; then cd " + directory + " && git pull ; else git clone " + this.getRepository() + " " + directory + " ; fi"));
		}
		
		distExec.addCommand(new ShellExecCommand("cd " + directory + " && git checkout " + version));
	}
	
	public void addRemoteExportCommands(DistributedShellExec distExec, String branchType, String version, String directory, boolean noCleanup)
	{
		if((distExec == null) || (branchType == null) || (version == null) || (directory == null)) {
			throw new NullPointerException();
		}
		if(branchType.isEmpty()) {
			throw new IllegalArgumentException("Missing branch type");
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(directory.isEmpty()) {
			throw new IllegalArgumentException("Missing directory");
		}
		
		StringBuilder export = new StringBuilder();

		export
			.append("mkdir ").append(directory)
			.append(" && git archive --format=tar --remote=").append(this.getRepository()).append(" ").append(version)
			.append(" | tar -vxC ").append(directory);
		
		if(noCleanup) {
			distExec.addCommand(new ShellExecCommand("if [ ! -d " + directory + " ]; then " + export + " ; fi"));
		} else {
			distExec.addCommand(new ShellExecCommand("if [ -d " + directory + " ]; then rm -rf " + directory + " ; fi"));
			distExec.addCommand(new ShellExecCommand(export.toString()));
		}
	}

	private void updateLocal() throws ScmClientException, IOException
	{
		if(this.getRepoDir().exists()) {
			this.runProcess("git", "checkout", "master");
			this.runProcess("git", "pull");
		} else {
			this.runProcess("git", "clone", this.getRepository(), this.getRepoDir().getCanonicalPath());
		}
	}

	public List<String> getBranches() throws ScmClientException, IOException
	{
		List<String> branches = new Vector<String>();

		this.updateLocal();

		List<String> out = this.runProcess("git", "branch", "-r");

		for(String line:out) {

			line = line.startsWith("origin/") ? line.substring(7) : line;

			if(line.startsWith("HEAD ") || line.equals("master")) {
				continue;
			}

			if(!this.isBranchValid(line)) {
				continue;
			}

			branches.add(line);
		}

		return branches;
	}

	public List<String> getTags() throws ScmClientException, IOException
	{
		List<String> tags = new Vector<String>();

		this.updateLocal();

		List<String> out = this.runProcess("git", "tag", "-l");

		for(String line:out) {
			if(!this.isTagValid(line)) {
				continue;
			}

			tags.add(line);
		}

		return tags;
	}

	public List<String> getMyBranches() throws ScmClientException, IOException
	{
		List<String> branches = new Vector<String>();

		this.updateLocal();

		List<String> out = this.runProcess("git", "branch", "-r");

		for(String line:out) {

			line = line.startsWith("origin/") ? line.substring(7) : line;

			if(line.startsWith("HEAD ") || line.equals("master")) {
				continue;
			}
			if(!this.isMyBranchValid(line)) {
				continue;
			}

			branches.add(line);
		}

		return branches;
	}

	public String createBranchFromMainBranch(String version) throws ScmClientException, IOException
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(!this.isBranchValid(version)) {
			throw new IllegalArgumentException("Invalid branch version format");
		}

		this.updateLocal();

		this.runProcess("git", "branch", version);
		this.runProcess("git", "push", "-u", "origin", version);

		return version;
	}

	public String createTagFromBranch(String version) throws ScmClientException, IOException
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(!this.isBranchValid(version)) {
			throw new IllegalArgumentException("Invalid branch version format");
		}

		this.updateLocal();

		String tag = this.getNextTag(version);

		this.runProcess("git", "checkout", version);
		this.runProcess("git", "tag", tag);
		this.runProcess("git", "push", "-u", "origin", tag);

		return tag;
	}
	
	public String createTagFromMainBranch(String version) throws ScmClientException, IOException
	{
		if(!this.isTagValid(version)) {
			throw new IllegalArgumentException("Invalid tag version format");
		}
		
		this.updateLocal();

		String tag = this.getNextTag(version);

		this.runProcess("git", "tag", tag);
		this.runProcess("git", "push", "-u", "origin", tag);

		return tag;
	}

	public Integer getBranchLastChangedRevision(String version) throws ScmClientException, IOException
	{
		throw new UnsupportedOperationException("This client does not support get last branch revision operation");
	}
	
	public Integer getTagLastChangedRevision(String version) throws ScmClientException, IOException
	{
		throw new UnsupportedOperationException("This client does not support get last tag revision operation");
	}
	
	public void fixTagExternalToRevision(String version, String external, Integer revision) throws ScmClientException, IOException
	{
		throw new UnsupportedOperationException("This client does not support externals");
	}

	public void fixBranchExternalToRevision(String version, String external, Integer revision) throws ScmClientException, IOException
	{	
		throw new UnsupportedOperationException("This client does not support externals");
	}
}