/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.scm;

import uk.co.bedican.snooze.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

public class SvnClient extends AbstractScmClient
{
	private static Pattern myBranchVersionPathPattern = Pattern.compile("((?<!rc)[^\\s]+)\\/");
	private static Pattern branchVersionPathPattern = Pattern.compile("((rc)?([0-9]+\\.[0-9]+))\\/");
	private static Pattern tagVersionPathPattern = Pattern.compile("([0-9]+\\.[0-9]+(?:-p[0-9]+)?)?\\/");
	
	public void addRemoteSwitchingCommands(DistributedShellExec distExec, String branchType, String version, String directory, boolean forceCheckout)
	{
		if((distExec == null) || (branchType == null) || (version == null) || (directory == null)) {
			throw new NullPointerException();
		}
		if(branchType.isEmpty()) {
			throw new IllegalArgumentException("Missing branch type");
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(directory.isEmpty()) {
			throw new IllegalArgumentException("Missing directory");
		}
		
		StringBuilder params = new StringBuilder();
		params.append(this.getRepository());

		if(branchType.equals("tag")) {
			params.append("/tags/");
		} else if(branchType.equals("mybranch")) {
			params.append("/branches/");
		} else {
			params.append("/branches/releases/");
		}
		
		params.append(version).append(" ").append(directory);

		if(forceCheckout) {
			distExec.addCommand(new ShellExecCommand("rm -rf " + directory));
			distExec.addCommand(new ShellExecCommand("svn co " + params));
		} else {
			distExec.addCommand(new ShellExecCommand("if [ -d " + directory + " ]; then svn switch " + params + " ; else svn co " + params + " ; fi"));
			distExec.addCommand(new ShellExecCommand("svn up " + directory));
		}
	}
	
	public void addRemoteExportCommands(DistributedShellExec distExec, String branchType, String version, String directory, boolean noCleanup)
	{
		if((distExec == null) || (branchType == null) || (version == null) || (directory == null)) {
			throw new NullPointerException();
		}
		if(branchType.isEmpty()) {
			throw new IllegalArgumentException("Missing branch type");
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(directory.isEmpty()) {
			throw new IllegalArgumentException("Missing directory");
		}

		StringBuilder params = new StringBuilder();
		params.append(this.getRepository());

		if(branchType.equals("tag")) {
			params.append("/tags/");
		} else if(branchType.equals("mybranch")) {
			params.append("/branches/");
		} else {
			params.append("/branches/releases/");
		}
		
		params.append(version).append(" ").append(directory);
		
		if(noCleanup) {
			distExec.addCommand(new ShellExecCommand("if [ ! -d " + directory + " ]; then svn export " + params + " ; fi"));
		} else {
			distExec.addCommand(new ShellExecCommand("if [ -d " + directory + " ]; then rm -rf " + directory + " ; fi"));
			distExec.addCommand(new ShellExecCommand("svn export " + params));
		}
	}
	
	public List<String> getBranches() throws ScmClientException, IOException
	{
		List<String> branches = new Vector<String>();

		List<String> out = this.runProcess("svn", "ls", this.getRepository() + "/branches/releases");

		Matcher matcher;

		for(String line:out) {
			matcher = this.branchVersionPathPattern.matcher(line);
			if(matcher.matches()) {
				branches.add(matcher.group(1));
			}
		}

		return this.sortVersions(branches);
	}

	public List<String> getTags() throws ScmClientException, IOException
	{
		List<String> tags = new Vector<String>();

		List<String> out = this.runProcess("svn", "ls", this.getRepository() + "/tags");

		Matcher matcher;

		for(String line:out) {
			matcher = this.tagVersionPathPattern.matcher(line);
			if(matcher.matches()) {
				tags.add(matcher.group(1));
			}
		}

		return this.sortVersions(tags);
	}

	public List<String> getMyBranches() throws ScmClientException, IOException
	{
		List<String> branches = new Vector<String>();

		List<String> out = this.runProcess("svn", "ls", this.getRepository() + "/branches");

		Matcher matcher;

		for(String line:out) {
			matcher = this.myBranchVersionPathPattern.matcher(line);
			if((matcher.matches()) && (!line.trim().toLowerCase().equals("releases/"))) {
				branches.add(matcher.group(1));
			}
		}

		return this.sortVersions(branches);
	}

	public String createBranchFromMainBranch(String version) throws ScmClientException, IOException
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(!this.isBranchValid(version)) {
			throw new IllegalArgumentException("Invalid branch version format");
		}

		this.runProcess("svn", "copy", this.getRepository() + "/" + this.getMainBranch(), this.getRepository() + "/branches/releases/" + version, "-m", "Creating release branch " + version);

		return version;
	}

	public String createTagFromBranch(String version) throws ScmClientException, IOException
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(!this.isBranchValid(version)) {
			throw new IllegalArgumentException("Invalid branch version format");
		}

		String tag = this.getNextTag(version);

		this.runProcess("svn", "copy", this.getRepository() + "/branches/releases/" + version, this.getRepository() + "/tags/" + tag, "-m", "Creating tag " + tag + " from branch " + version);

		return tag;
	}
	
	public String createTagFromMainBranch(String version) throws ScmClientException, IOException
	{
		if(!this.isTagValid(version)) {
			throw new IllegalArgumentException("Invalid tag version format");
		}
		
		String tag = this.getNextTag(version);

		this.runProcess("svn", "copy", this.getRepository() + "/" + this.getMainBranch(), this.getRepository() + "/tags/" + tag, "-m", "Creating tag " + tag + " from " + this.getMainBranch());

		return tag;
	}

	public Integer getBranchLastChangedRevision(String version) throws ScmClientException, IOException
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(!this.isBranchValid(version)) {
			throw new IllegalArgumentException("Invalid branch version format");
		}
		
		Integer revision = this.getLastChangedRevision(this.getRepository() + "/branches/releases/" + version);
		
		if(revision == null) {
			throw new ScmClientException("Failed to obtain revision for release branch " + version);
		}
		
		return revision;
	}
	
	public Integer getTagLastChangedRevision(String version) throws ScmClientException, IOException
	{
		if(version == null) {
			throw new NullPointerException();
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(!this.isTagValid(version)) {
			throw new IllegalArgumentException("Invalid tag version format");
		}
		
		Integer revision = this.getLastChangedRevision(this.getRepository() + "/tags/" + version);
		
		if(revision == null) {
			throw new ScmClientException("Failed to obtain revision for tag " + version);
		}
		
		return revision;
	}

	private Integer getLastChangedRevision(String path) throws ScmClientException, IOException
	{
		if(path == null) {
			throw new NullPointerException();
		}
		if(path.isEmpty()) {
			throw new IllegalArgumentException("Missing path");
		}

		List<String> out = this.runProcess("svn", "info", path);

		for(String line:out) {
			if(line.toLowerCase().startsWith("last changed rev: "))
			{
				try {
					return Integer.valueOf(line.substring(18));
				} catch(NumberFormatException nfe) {
					// Just continue
				}
			}
		}

		return null;
	}
	
	public void fixTagExternalToRevision(String version, String external, Integer revision) throws ScmClientException, IOException
	{
		this.fixExternalToRevision(this.getRepository() + "/tags", version, external, revision);
	}

	public void fixBranchExternalToRevision(String version, String external, Integer revision) throws ScmClientException, IOException
	{	
		this.fixExternalToRevision(this.getRepository() + "/branches/releases", version, external, revision);
	}
	
	private void fixExternalToRevision(String path, String version, String external, Integer revision) throws ScmClientException, IOException
	{
		if((path == null) || (version == null) || (external == null) || (revision == null)) {
			throw new NullPointerException();
		}
		if(path.isEmpty()) {
			throw new IllegalArgumentException("Missing path");
		}
		if(version.isEmpty()) {
			throw new IllegalArgumentException("Missing version");
		}
		if(external.isEmpty()) {
			throw new IllegalArgumentException("Missing external");
		}

		String svnPath = path + "/" + version + "/" + external;

		// Generate externals file

		File tmpFile = File.createTempFile(version + "-" + revision, ".externals");
		PrintWriter tmpFileWriter = new PrintWriter(new FileOutputStream(tmpFile), true);

		List<String> out = this.runProcess("svn", "propget", "svn:externals", svnPath);

		for(String line:out) {
			if(!line.isEmpty()) {
				tmpFileWriter.println(line.replaceFirst("[^\\s]+", "$0 -r" + revision.toString()));
			}
		}

		tmpFileWriter.close();

		// We cant use propset with svn:externals directly with absolute uris (its unsupported)
		// Therefore we have to do a non-recursive checkout, set the externals and then commit. 
		// This is a bit horrible but a necessary evil.

		// Re-use the unique name of our already created tmp file.
		String tmpDir = tmpFile.getAbsolutePath() + ".checkout";

		StringBuffer command = new StringBuffer();

		command
			.append("if [ -d ").append(tmpDir).append("]; then ")
			.append("rm -rf ").append(tmpDir).append("; fi && ")
			.append("mkdir ").append(tmpDir).append(" && ")
			.append("cd ").append(tmpDir).append(" && ")
			.append("mkdir -p ").append(external).append(" && ")
			.append("svn co -N ").append(svnPath).append(" ").append(external).append(" && ")
			.append("svn propset svn:externals -F ").append(tmpFile.getAbsolutePath()).append(" ").append(external).append(" && ")
			.append("svn ci ").append(external).append(" -m 'Fixing external to revision ").append(revision.toString()).append(" ' && ")
			.append("cd .. && ")
			.append("rm -rf ").append(tmpDir);

		this.runProcess("bash", "-c", command.toString());

		tmpFile.delete();
	}
}