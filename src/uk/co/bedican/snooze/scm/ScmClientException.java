/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.scm;

public class ScmClientException extends Exception
{
	public ScmClientException(String message) { super(message); }
	public ScmClientException(String message, Throwable t) { super(message, t); }
	public ScmClientException(Throwable t) { super(t.getMessage(), t); }
}