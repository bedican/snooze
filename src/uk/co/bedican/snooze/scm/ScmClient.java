/**
 * (c) Copyright 2012 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze.scm;

import uk.co.bedican.snooze.*;
import java.io.*;
import java.util.*;

public interface ScmClient
{
	public void init(String repository, String mainBranch, File localDir) throws ScmClientException;
	
	public String getRepository();
	public String getMainBranch();
	public File getRepoDir();

	public void addRemoteSwitchingCommands(DistributedShellExec distExec, String branchType, String version, String directory, boolean forceCheckout);
	public void addRemoteExportCommands(DistributedShellExec distExec, String branchType, String version, String directory, boolean noCleanup);
	
	public boolean isBranchValid(String version);
	public boolean isTagValid(String version);
	public boolean isMyBranchValid(String version);
	public boolean isVersionValid(String version);

	public List<String> getBranches() throws ScmClientException, IOException;
	public List<String> getTags() throws ScmClientException, IOException;
	public List<String> getMyBranches() throws ScmClientException, IOException;

	public boolean isBranch(String version) throws ScmClientException, IOException;
	public boolean isTag(String version) throws ScmClientException, IOException;
	public boolean isMyBranch(String branch) throws ScmClientException, IOException;

	public String getCurrentBranch() throws ScmClientException, IOException;
	public String getNextBranch() throws ScmClientException, IOException;
	public String getNextTag(String version) throws ScmClientException, IOException;
	public String getNextTag() throws ScmClientException, IOException;

	public String createBranchFromMainBranch(String version) throws ScmClientException, IOException;
	public String createTagFromBranch(String version) throws ScmClientException, IOException;
	public String createTagFromMainBranch(String version) throws ScmClientException, IOException;

	public Integer getBranchLastChangedRevision(String version) throws ScmClientException, IOException;	
	public Integer getTagLastChangedRevision(String version) throws ScmClientException, IOException;
	public void fixBranchExternalToRevision(String version, String external, Integer revision) throws ScmClientException, IOException;
	public void fixTagExternalToRevision(String version, String external, Integer revision) throws ScmClientException, IOException;
}