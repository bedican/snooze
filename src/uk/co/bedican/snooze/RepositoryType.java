/**
 * (c) Copyright 2012 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze;

import uk.co.bedican.snooze.scm.*;
import java.io.*;

public enum RepositoryType
{
	SVN("svn", SvnClient.class), 
	GIT("git", GitClient.class);
	
	private String name;
	private Class<?> clazz;
	
	RepositoryType(String name, Class<?> clazz)
	{
		this.name = name;
		this.clazz = clazz;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public ScmClient newClient(String repository, String mainBranch, File localRepo)
	{
		if((repository == null) || (mainBranch == null) || (localRepo == null)) {
			throw new NullPointerException();
		}
		if(repository.isEmpty()) {
			throw new IllegalArgumentException("repository is missing");
		}
		if(mainBranch.isEmpty()) {
			throw new IllegalArgumentException("mainBranch is missing");
		}
		
		ScmClient client = null;

		try
		{
			client = (ScmClient)this.clazz.newInstance();
			client.init(repository, mainBranch, localRepo);
		}
		catch(Exception e)
		{
			throw new RuntimeException("Failed to create ScmClient instance", e);
		}

		return client;	
	}
	
	public static RepositoryType getType(String name)
	{
		for(RepositoryType type:RepositoryType.values()) {
			if(type.getName().equals(name)) {
				return type;
			}
		}

		return null;
	}
}