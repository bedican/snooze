/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.snooze;

public class ShellExecCommand
{
	private String command;

	private boolean stopOnError = true;
	private boolean outputBuffering = false;
	private boolean singleServerMode = false;
	
	private String rollbackCommand = null;

	public ShellExecCommand(String command)
	{
		this(command, true);
	}
	public ShellExecCommand(String command, boolean stopOnError)
	{
		if(command == null) {
			throw new NullPointerException();
		}
		if(command.isEmpty()) {
			throw new IllegalArgumentException("Missing command");
		}

		this.command = command;
		this.stopOnError = stopOnError;
	}
	public ShellExecCommand(String command, String rollbackCommand)
	{
		this(command, rollbackCommand, true);
	}
	public ShellExecCommand(String command, String rollbackCommand, boolean stopOnError)
	{
		if((command == null) || (rollbackCommand == null)) {
			throw new NullPointerException();
		}
		if(command.isEmpty()) {
			throw new IllegalArgumentException("Missing command");
		}
		if(rollbackCommand.isEmpty()) {
			throw new IllegalArgumentException("Missing rollback command");
		}

		this.command = command;
		this.rollbackCommand = rollbackCommand;
		this.stopOnError = stopOnError;
	}

	public String getCommand()
	{
		return this.command;
	}

	public boolean canRollback()
	{
		return (this.rollbackCommand != null);
	}
	public String getRollbackCommand()
	{
		return this.rollbackCommand;
	}

	public boolean getStopOnError()
	{
		return this.stopOnError;
	}
	public boolean getOutputBuffering()
	{
		return this.outputBuffering;
	}
	public boolean getSingleServerMode()
	{
		return this.singleServerMode;
	}
	
	public void setStopOnError(boolean stopOnError)
	{
		this.stopOnError = stopOnError;
	}
	public void setOutputBuffering(boolean outputBuffering)
	{
		this.outputBuffering = outputBuffering;
	}
	public void setSingleServerMode(boolean singleServerMode)
	{
		this.singleServerMode = singleServerMode;
	}
}